﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Track
{
    /// <summary>
    /// Interaction logic for ViewPPM.xaml
    /// </summary>
    public partial class ViewPPM : UserControl
    {

        public static ViewPPM getViewPPM(int ppm_ID)
        {

            DataMap.IDataMapFactory factory = DataMap.DataMapFactoryBase.factory<TrackFactory>();
            Track.Model.PPM.PPM ppm = factory.get<Track.Model.PPM.PPM>(ppm_ID);

            ViewPPM viewPPM = new ViewPPM();
            viewPPM.DataContext = ppm;

            return viewPPM;
        }


        public ViewPPM()
        {
            InitializeComponent();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(DataContext.ToString());
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            ((Track.Model.PPM.PPM)DataContext).commit();
        }
    }
}
