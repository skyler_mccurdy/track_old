﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Track.MainToolBar
{
    /// <summary>
    /// Interaction logic for PPMs.xaml
    /// </summary>
    public partial class PPMs : UserControl
    {
        public class Model
        {
            public IEnumerable<Track.Model.PPM.PPM> ppms { get; set; }


            public Model()
            {
                ppms = new List<Track.Model.PPM.PPM>();
            }
        }

        protected void buildModel()
        {
            
        }

        public PPMs()
        {
            buildModel();
            InitializeComponent();

        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int ppm_ID = ((Track.Model.PPM.PPM)lbxPPMs.SelectedItem).ppm_ID;

            ViewPPM viewPPM = ViewPPM.getViewPPM(ppm_ID);
            ((MainWindow)GetTopLevelControl(this)).changeMainBodyTo(viewPPM);
        }


        public DependencyObject GetTopLevelControl(DependencyObject control)
        {
            DependencyObject tmp = control;
            DependencyObject parent = null;
            while ((tmp = VisualTreeHelper.GetParent(tmp)) != null)
            {
                parent = tmp;
            }
            return parent;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Model model = new Model();

            DataMap.IDataMapFactory factory = DataMap.DataMapFactoryBase.factory<TrackFactory>();

            model.ppms = Track.Model.PPM.PPM.getAll(factory);

            this.DataContext = model;
        }
    }
}
