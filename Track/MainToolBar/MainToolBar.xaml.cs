﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Track.MainToolBar
{
    /// <summary>
    /// Interaction logic for MainToolBar.xaml
    /// </summary>
    public partial class MainToolBar : UserControl
    {
        public MainToolBar()
        {
            InitializeComponent();
        }


        private void TreeViewItem_MouseLeftUp(object sender, MouseButtonEventArgs e)
        {
            ViewPPM viewPPM = ViewPPM.getViewPPM(1);


            ((MainWindow)GetTopLevelControl(this)).changeMainBodyTo(viewPPM);
        }
        

        public DependencyObject GetTopLevelControl(DependencyObject control)
        {
            DependencyObject tmp = control;
            DependencyObject parent = null;
            while ((tmp = VisualTreeHelper.GetParent(tmp)) != null)
            {
                parent = tmp;
            }
            return parent;
        }

        private void TabControl_MouseEnter(object sender, MouseEventArgs e)
        {
            ((MainWindow)GetTopLevelControl(this)).expandMainToolBar();
        }

        private void TabControl_MouseLeave(object sender, MouseEventArgs e)
        {
            Point m = e.GetPosition(this);
            Rect r = ((MainWindow)GetTopLevelControl(this)).getMainToolBarSize();
            if (m.X < 0 || m.X > r.Width || m.Y < 0 || m.Y > r.Height)
            {
                ((MainWindow)GetTopLevelControl(this)).colapseMainToolBar();
            }
        }

        private void MenuItem_ViewPPM_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)GetTopLevelControl(this)).changeMainBodyTo(new ViewPPM());
        }

        private void MenuItem_EditPPM_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)GetTopLevelControl(this)).changeMainBodyTo(new EditPPM());
        }
    }
}
