﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

using Track.BlendHelper;


namespace Track
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private static readonly string logTarget =
            "Data Source=TEST-PC;Initial Catalog=ErrorLog;Integrated Security=SSPI;";

        public MainWindow()
        {
            InitializeComponent();
            //LogIt.LogIt.targets.Add(new LogIt.Targets.DatabaseTarget(logTarget));
            LogIt.LogIt.targets.Add(new LogIt.Targets.ConsoleTarget());
        }

        private void MdiContainer_Loaded(object sender, RoutedEventArgs e) {
            /*Window win = Window.GetWindow(this);
            if (win != null)
            {
                win.Activated += new EventHandler(MdiContainer_Activated);
                win.Deactivated += new EventHandler(MdiContainer_Deactivated);
            }

            windowCanvas.Width = windowCanvas.ActualWidth;
            windowCanvas.Height = windowCanvas.ActualHeight;

            windowCanvas.VerticalAlignment = VerticalAlignment.Top;
            windowCanvas.HorizontalAlignment = HorizontalAlignment.Left;

            InvalidateSize();*/


        }

        private void MenuItem_About_Click(object sender, RoutedEventArgs e)
        {
            changeMainBodyTo(new About());

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
        }

        private void MenuImportPPM_Click(object sender, RoutedEventArgs e)
        {
            Window win = new ImportPPM();
            win.Show();
        }



        private UserControl mainBody_Target = null;

        public void changeMainBodyTo(UserControl uc)
        {
            mainBody_Target = uc;

            Storyboard sb;
            sb = (Storyboard)this.Resources["MainBody_FadeOut"];
            sb.Completed += mainBody_FadeOut_Completed;
            sb.Begin(this);
            
            colapseMainToolBar();
            
        }

        void mainBody_FadeOut_Completed(object sender, EventArgs e)
        {
            MainBody.Children.Clear();
            if (mainBody_Target != null)
            {
                MainBody.Children.Add(mainBody_Target);

                Storyboard sb;
                sb = (Storyboard)this.Resources["MainBody_FadeIn"];
                sb.Begin(this);
            }
        }

        public Rect getMainToolBarSize()
        {
            return new Rect(0, 0, mainToolBar.ActualWidth, mainToolBar.ActualHeight);
        }

        public void expandMainToolBar()
        {
            if (MainToolbarCol.Width.Value < 40)
            {
                Storyboard sb = (Storyboard)this.Resources["MainToolBar_Expand"];
                sb.Begin(this);
            }
        }

        void toolBar_Expand_Completed(object sender, EventArgs e)
        {
            Storyboard sb = FindResource("MainToolBar_Expand") as Storyboard;
            foreach (var c in sb.Children)
            {
                if (c.GetType() == typeof(GridLengthAnimation))
                {
                    GridLengthAnimation anim = c as GridLengthAnimation;
                    MainToolbarCol.Width = anim.To;
                }
            }
        }

        public void colapseMainToolBar()
        {
            if (MainToolbarCol.Width.Value > 40)
            {
                Storyboard sb = (Storyboard)this.Resources["MainToolBar_Colapse"];
                foreach (var c in sb.Children)
                {
                    if (c.GetType() == typeof(GridLengthAnimation))
                    {
                        GridLengthAnimation anim = c as GridLengthAnimation;
                        anim.From = MainToolbarCol.Width;
                    }
                }
                sb.Begin(this);
            }
        }

        void toolBar_Colapse_Completed(object sender, EventArgs e)
        {
            Storyboard sb = FindResource("MainToolBar_Colapse") as Storyboard;
            foreach (var c in sb.Children)
            {
                if (c.GetType() == typeof(GridLengthAnimation))
                {
                    GridLengthAnimation anim = c as GridLengthAnimation;
                    MainToolbarCol.Width = anim.To;
                }
            }
        }

    }
}
