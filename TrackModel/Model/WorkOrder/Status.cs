﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Model.WorkOrder
{
    [DataMap.Sql.Procedure.Select("exec WorkOrder.workOrderStatus_getByID null,@status_ID")]
    [DataMap.Sql.Procedure.Update("exec WorkOrder.workOrderStatus_update null,@status_ID,@name,@displayOrder")]
    [DataMap.Sql.Procedure.Insert("exec WorkOrder.workOrderStatus_insert null,@status_ID,@name,@displayOrder")]
    [DataMap.Sql.Procedure.Delete("exec WorkOrder.workOrderStatus_delete null,@status_ID")]
    public class Status : DataMap.ObjectBase
    {
        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Select.Parameter("@status_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Update.Parameter("@status_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Delete.Parameter("@status_ID", System.Data.SqlDbType.Int)]
        public int status_ID
        {
            get
            {
                return _status_ID;
            }

            set
            {
                if (_status_ID != value)
                {
                    _status_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _status_ID = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@name", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@name", System.Data.SqlDbType.VarChar)]
        public string name
        {
            get
            {
                return _name;
            }

            set
            {
                if (_name != value)
                {
                    _name = value;
                    onPropertyChanged();
                }
            }
        }
        private string _name = null;

        [DataMap.Sql.Column("displayOrder")]
        [DataMap.Sql.Procedure.Update.Parameter("@displayOrder", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@displayOrder", System.Data.SqlDbType.Int)]
        public int order
        {
            get
            {
                return _order;
            }

            set
            {
                if (_order != value)
                {
                    _order = value;
                    onPropertyChanged();
                }
            }
        }
        private int _order = -1;

        public Status()
        {
            isNew = true;
            isModified = true;
        }

        public Status(int status_ID)
        {
            this.status_ID = status_ID;
            isNew = false;
            isModified = false;
        }
    }
}
