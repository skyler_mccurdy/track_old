﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Model.WorkOrder
{
    [DataMap.Sql.Procedure.Select("exec WorkOrder.workOrder_getByID null,@workOrder_ID")]
    [DataMap.Sql.Procedure.Update("exec WorkOrder.workOrder_update null,@workOrder_ID,@name,@scheduledStartOn,@feedback,@status_ID,@mp2_WO,@ppm_ID")]
    [DataMap.Sql.Procedure.Insert("exec WorkOrder.workOrder_insert null,@name,@scheduledStartOn,@feedback,@status_ID,@mp2_WO,@ppm_ID")]
    [DataMap.Sql.Procedure.Delete("exec WorkOrder.workOrder_delete null,@workOrder_ID")]
    public class WorkOrder : DataMap.ObjectBase
    {
        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Select.Parameter("@workOrder_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Update.Parameter("@workOrder_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Delete.Parameter("@sworkOrder_ID", System.Data.SqlDbType.Int)]
        public int workOrder_ID
        {
            get
            {
                return _workOrder_ID;
            }

            set
            {
                if (_workOrder_ID != value)
                {
                    _workOrder_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _workOrder_ID = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@name", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@name", System.Data.SqlDbType.VarChar)]
        public string name
        {
            get
            {
                return _name;
            }

            set
            {
                if (_name != value)
                {
                    _name = value;
                    onPropertyChanged();
                }
            }
        }
        private string _name = null;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@scheduledStartOn", System.Data.SqlDbType.DateTime2)]
        [DataMap.Sql.Procedure.Insert.Parameter("@scheduledStartOn", System.Data.SqlDbType.DateTime2)]
        public DateTime? scheduledStartOn
        {
            get
            {
                return _scheduledStartOn;
            }

            set
            {
                if (_scheduledStartOn != value)
                {
                    _scheduledStartOn = value;
                    onPropertyChanged();
                }
            }
        }
        private DateTime? _scheduledStartOn = null;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@feedback", System.Data.SqlDbType.Text)]
        [DataMap.Sql.Procedure.Insert.Parameter("@feedback", System.Data.SqlDbType.Text)]
        public string feedback
        {
            get
            {
                return _feedback;
            }

            set
            {
                if (_feedback != value)
                {
                    _feedback = value;
                    onPropertyChanged();
                }
            }
        }
        private string _feedback = null;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@status_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@status_ID", System.Data.SqlDbType.Int)]
        public int status_ID
        {
            get
            {
                return _status_ID;
            }

            set
            {
                if (_status_ID != value)
                {
                    _status_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _status_ID = -1;

        [DataMap.Sql.Column("mp2_WO")]
        [DataMap.Sql.Procedure.Update.Parameter("@mp2_WO", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@mp2_WO", System.Data.SqlDbType.VarChar)]
        public string mp2_workOrder_ID
        {
            get
            {
                return _mp2_workOrder_ID;
            }

            set
            {
                if (_mp2_workOrder_ID != value)
                {
                    _mp2_workOrder_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private string _mp2_workOrder_ID = null;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@ppm_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@ppm_ID", System.Data.SqlDbType.Int)]
        public int ppm_ID
        {
            get
            {
                return _ppm_ID;
            }

            set
            {
                if (_ppm_ID != value)
                {
                    _ppm_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _ppm_ID = -1;


        public PPM.PPM ppm
        {
            get
            {
                return factory.get<PPM.PPM>(ppm_ID);
            }
        }

        public WorkOrder()
        {
            isNew = true;
            isModified = true;
        }

        public WorkOrder(int workOrder_ID)
        {
            this.workOrder_ID = workOrder_ID;
            isNew = false;
            isModified = false;
        }
    }
}
