﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Track.Model.UserSecurity
{
    
    
    
    public class Token//:Track.Model.Object
    {
/*
        protected static LogIt.ILog securityLog = LogIt.LogIt.getLog("Security");

        public override bool commit()
        {
            throw new NotImplementedException();
        }

        public override bool refresh()
        {
            getTokenUserInfo();
            
            return (token!=null);
        }

        public override bool delete()
        {
            throw new NotImplementedException();
        }

        private string _token = null;
        public string token
        {
            get
            {
                getTokenUserInfo();
                return _token;
            }

            private set
            {
                if (_token != value)
                {
                    _token = value;
                    OnPropertyChanged();
                }
            }
        }

        private DateTime? _authenticatedOn = null;
        public DateTime? authenticatedOn
        {
            get
            {
                getTokenUserInfo();
                return _authenticatedOn;
            }
            private set
            {
                if (_authenticatedOn != value)
                {
                    _authenticatedOn = value;
                    OnPropertyChanged();
                }
            }
        }

        private int? _person_ID = null;
        public int? person_ID
        {
            get
            {
                getTokenUserInfo();
                return _person_ID;
            }
            private set
            {
                if (_person_ID != value)
                {
                    _person_ID = value;
                    OnPropertyChanged();
                    dbState = DB_STATE.MODIFIED;
                }
            }
        }

        private string _name = null;
        public string name
        {
            get
            {
                getTokenUserInfo();
                return _name;
            }
            private set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged();
                    dbState = DB_STATE.MODIFIED;
                }
            }
        }

        private string _clientMachine = null;
        public string clientMachine
        {
            get
            {
                getTokenUserInfo();
                return _clientMachine;
            }
            private set
            {
                if (_clientMachine != value)
                {
                    _clientMachine = value;
                    OnPropertyChanged();
                    dbState = DB_STATE.MODIFIED;
                }
            }
        }

        private DateTime? lastUpdate = null;
        private void getTokenUserInfo()
        {
            if (lastUpdate != null && (DateTime.Now - lastUpdate) < new TimeSpan(0, 0, 1))
                return;
            lastUpdate = DateTime.Now;

            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            try
            {
                cmd = new SqlCommand(
                    "exec UserSecurity.token_getUserFromToken @token;",
                    connection);
                cmd.Parameters.Add("@token", System.Data.SqlDbType.VarChar).Value = token;

                rdr = cmd.ExecuteReader();
                rdr.Read();


                token = getString(rdr, "token_ID");
                authenticatedOn = getDateTime(rdr, "authenticatedOn");
                person_ID = getInt(rdr, "person_ID");
                name = getString(rdr, "name");
                clientMachine = getString(rdr, "clientMachine");

                
            }
            catch (Exception ex)
            {
                sqlLog.error("Windows user authentication error", ex);
                securityLog.error("Unable to authenticate user.", ex);

                token = null;
                authenticatedOn = null;
                person_ID = null;
                name = null;
                clientMachine = null;
            }
            finally
            {
                if (rdr != null)
                    rdr.Close();
                if (cmd != null)
                {
                    cmd.Cancel();
                }

            }
        }

        public bool canRead(string resourceName, string additional)
        {
            SqlCommand cmd = null;
            SqlDataReader rdr = null;

            try
            {
                cmd = new SqlCommand(
                    "exec UserSecurity.permission_canRead @token,@resourceName",
                    connection);
                cmd.Parameters.Add("@token", System.Data.SqlDbType.VarChar).Value = token;
                cmd.Parameters.Add("@resourceName", System.Data.SqlDbType.VarChar).Value = resourceName;

                if(additional!=null){
                    cmd.CommandText +=",@additional";
                    cmd.Parameters.Add("@additional", System.Data.SqlDbType.VarChar).Value = additional;
                }

                rdr = cmd.ExecuteReader();
                rdr.Read();

                return getBool(rdr, "CanRead");


            }
            catch (Exception ex)
            {
                sqlLog.error("Windows user authentication error", ex);
                securityLog.error("Unable to authenticate user.", ex);

                return false;
            }
            finally
            {
                if (rdr != null)
                    rdr.Close();
                if (cmd != null)
                {
                    cmd.Cancel();
                }

            }
        }

        public Token(string token)
        {
            this.token = token;
            getTokenUserInfo();
        }


        private static Token _currentToken = null;
        public static Token currentToken
        {
            get
            {
                return _currentToken;
            }

            private set
            {
                _currentToken = value;
            }
        }

        public static Token authenticate(string username, string clientMachine)
        {
            SqlCommand cmd=null;
            SqlDataReader rdr=null;

            try
            {
                cmd = new SqlCommand(
                    "exec UserSecurity.authenticateWindowsUser @userName,@clientMachine;", 
                    connection);
                cmd.Parameters.Add("@userName", System.Data.SqlDbType.VarChar).Value = username;
                cmd.Parameters.Add("@clientMachine", System.Data.SqlDbType.VarChar).Value = clientMachine;

                rdr = cmd.ExecuteReader();
                rdr.Read();
                string tmp = getString(rdr, "token_ID");

                currentToken = new Token(tmp);
                
                return currentToken;
            }
            catch (Exception ex)
            {
                sqlLog.error("Windows user authentication error", ex);
                securityLog.error("Unable to authenticate user.", ex);
                return null;
            }
            finally
            {
                if (rdr != null)
                    rdr.Close();
                if (cmd != null)
                {
                    cmd.Cancel();
                }

            }
        }*/

    }
}
