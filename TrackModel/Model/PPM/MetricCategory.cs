﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Model.PPM
{

    [DataMap.Sql.Procedure.Select("exec ppm.category_getByID null, @category_ID")]
    [DataMap.Sql.Procedure.Update("exec ppm.category_update null, @category_ID,@ppm_ID,@parent_ID,@displayOrder_ID,@name,@note")]
    [DataMap.Sql.Procedure.Insert("exec ppm.category_insert null, @ppm_ID,@parent_ID,@displayOrder_ID,@name,@note")]
    [DataMap.Sql.Procedure.Delete("exec ppm.category_delete null, @category_ID")]
    public class MetricCategory:DataMap.ObjectBase
    {
        [DataMap.Sql.Column("category_ID")]
        [DataMap.Sql.Procedure.Select.Parameter("@category_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Update.Parameter("@category_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Delete.Parameter("@category_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.SelectArray.Parameter("@category_ID", System.Data.SqlDbType.Int)]
        public int metricCategory_ID
        {
            get
            {
                return _metricCategory_ID;
            }

            set
            {
                if (_metricCategory_ID != value)
                {
                    _metricCategory_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _metricCategory_ID = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@ppm_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@ppm_ID", System.Data.SqlDbType.Int)]
        public int ppm_ID
        {
            get
            {
                return _ppm_ID;
            }

            set
            {
                if (_ppm_ID != value)
                {
                    _ppm_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _ppm_ID = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@parent_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@parent_ID", System.Data.SqlDbType.Int)]
        public int? parent_ID
        {
            get
            {
                return _parent_ID;
            }

            set
            {
                if (_parent_ID != value)
                {
                    _parent_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int? _parent_ID = null;

        [DataMap.Sql.Column("displayOrder")]
        [DataMap.Sql.Procedure.Update.Parameter("@displayOrder_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@displayOrder_ID", System.Data.SqlDbType.Int)]
        public int order
        {
            get
            {
                return _order;
            }

            set
            {
                if (_order != value)
                {
                    _order = value;
                    onPropertyChanged();
                }
            }
        }
        private int _order = 0;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@name", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@name", System.Data.SqlDbType.VarChar)]
        public string name
        {
            get
            {
                return _name;
            }

            set
            {
                if (_name != value)
                {
                    _name = value;
                    onPropertyChanged();
                }
            }

        }
        private string _name = null;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@note", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@note", System.Data.SqlDbType.VarChar)]
        public string note
        {
            get
            {
                return _note;
            }
            set
            {
                if (_note != value)
                {
                    _note = value;
                    onPropertyChanged();
                }
            }
        }
        private string _note = null;

        [DataMap.Sql.Procedure.SelectArray("exec PPM.category_forCategory null, @category_ID")]
        public IEnumerable<MetricCategory> categories
        {
            get
            {
                return dataInterface.getArray<MetricCategory>();
            }
        }

        [DataMap.Sql.Procedure.SelectArray("exec PPM.metric_forCategory null, @category_ID")]
        public IEnumerable<Metric> metrics
        {
            get
            {
                return dataInterface.getArray<Metric>();
            }
        }
       
        public MetricCategory()
        {
            isNew = true;
            isModified = true;
        }

        public MetricCategory(int metricCategory_ID)
        {
            this.metricCategory_ID = metricCategory_ID;
            isNew = false;
            isModified = false;
        }
    }
}
