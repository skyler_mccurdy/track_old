﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Model.PPM
{

    [DataMap.Sql.Procedure.Select("exec PPM.metric_getByID '',@metric_ID")]
    [DataMap.Sql.Procedure.Update("exec PPM.metric_update '',@metric_ID,@equipmentMetric_ID,@category_ID,@displayOrder,@note")]
    [DataMap.Sql.Procedure.Insert("exec PPM.metric_insert '',@equipmentMetric_ID,@category_ID,@displayOrder,@note")]
    [DataMap.Sql.Procedure.Delete("exec PPM.metric_delete '',@metric_ID")]
    public class Metric : DataMap.ObjectBase
    {
        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Select.Parameter("@metric_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Update.Parameter("@metric_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Delete.Parameter("@metric_ID", System.Data.SqlDbType.Int)]
        public int metric_ID
        {
            get
            {
                return _metric_ID;
            }

            set
            {
                if (_metric_ID != value)
                {
                    _metric_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _metric_ID = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@equipmentMetric_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@equipmentMetric_ID", System.Data.SqlDbType.Int)]
        public int equipmentMetric_ID
        {
            get
            {
                return _equipmentMetric_ID;
            }

            set
            {
                if (_equipmentMetric_ID != value)
                {
                    _equipmentMetric_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _equipmentMetric_ID = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@category_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@category_ID", System.Data.SqlDbType.Int)]
        public int category_ID
        {
            get
            {
                return _category_ID;
            }

            set
            {
                if (_category_ID != value)
                {
                    _category_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _category_ID = -1;

        [DataMap.Sql.Column("displayOrder")]
        [DataMap.Sql.Procedure.Update.Parameter("@displayOrder", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@displayOrder", System.Data.SqlDbType.Int)]
        public int order
        {
            get
            {
                return _order;
            }

            set
            {
                if (_order != value)
                {
                    _order = value;
                    onPropertyChanged();
                }
            }
        }
        private int _order = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@note", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@note", System.Data.SqlDbType.VarChar)]
        public string note
        {
            get
            {
                return _note;
            }

            set
            {
                if (_note != value)
                {
                    _note = value;
                    onPropertyChanged();
                }
            }
        }
        private string _note = null;

        public Model.Equipment.Metric getMetric()
        {
            return factory.get<Model.Equipment.Metric>(equipmentMetric_ID);
        }

        public Metric()
        {
            isNew = true;
            isModified = true;
        }

        public Metric(int metric_ID)
        {
            this.metric_ID = metric_ID;
        }
    }
}
