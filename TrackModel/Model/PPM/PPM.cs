﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using DataMap;

namespace Track.Model.PPM
{

   
    [DataMap.Sql.Procedure.Select("exec PPM.ppm_getByID null,@ppm_ID")]
    [DataMap.Sql.Procedure.Update("exec PPM.ppm_update null,@ppm_ID,@name,@head,@mp2_PPM")]
    [DataMap.Sql.Procedure.Insert("exec PPM.ppm_insert null,@name,@head,@mp2_PPM")]
    [DataMap.Sql.Procedure.Delete("exec PPM.ppm_delete null,@ppm_ID")]
    public class PPM:DataMap.ObjectBase
    {
        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Select.Parameter("@ppm_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@ppm_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Update.Parameter("@ppm_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Delete.Parameter("@ppm_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.SelectArray.Parameter("@ppm_ID", System.Data.SqlDbType.Int)]
        public int ppm_ID
        {
            get
            {
                return _ppm_ID;
            }

            set
            {
                if (_ppm_ID != value)
                {
                    _ppm_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _ppm_ID;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Insert.Parameter("@name", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Update.Parameter("@name", System.Data.SqlDbType.VarChar)]
        public string name
        {
            get
            {
                return _name;
            }

            set
            {
                if (_name != value)
                {
                    _name = value;
                    onPropertyChanged();
                }
            }
        }
        private string _name;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Insert.Parameter("@head", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Update.Parameter("@head", System.Data.SqlDbType.VarChar)]
        public string head
        {
            get
            {
                return _head;
            }
            set
            {
                if (_head != value)
                {
                    _head = value;
                    onPropertyChanged();
                }
            }
        }
        private string _head;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Insert.Parameter("@mp2_PPM", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Update.Parameter("@mp2_PPM", System.Data.SqlDbType.VarChar)]
        public string mp2_PPM
        {
            get
            {
                return _mp2_PPM;
            }

            set
            {
                if (_mp2_PPM != value)
                {
                    _mp2_PPM = value;
                    onPropertyChanged();
                }
            }
        }
        private string _mp2_PPM;


        [DataMap.Sql.Procedure.SelectArray("exec PPM.ppm_getAll null")]
        public static IEnumerable<PPM> getAll(DataMap.IDataMapFactory factory)
        {
            return factory.getArray<PPM>(typeof(PPM));
        }

        [DataMap.Sql.Procedure.SelectArray("exec PPM.category_forPPM null, @ppm_ID")]
        public IEnumerable<MetricCategory> categories
        {
            get
            {
                return dataInterface.getArray<MetricCategory>();
            }
        }

        public PPM()
        {
            isNew = true;
            isModified = true;
        }

        public PPM(int ppm_ID)
        {
            this.ppm_ID = ppm_ID;
            isNew = false;
            isModified = false;
        
        }

    }
}
