﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Model.Equipment
{
    
    [DataMap.Sql.Procedure.Select("exec Equipment.metricDomain_getByID null,@metricDomain_ID")]
    [DataMap.Sql.Procedure.Update("exec Equipment.metricDomain_update null,@metricDomain_ID,@metric_ID,@displayOrder,@name,@value")]
    [DataMap.Sql.Procedure.Insert("exec Equipment.metricDomain_insert null,@metric_ID,@displayOrder,@name,@value")]
    [DataMap.Sql.Procedure.Delete("exec Equipment.metricDomain_delete null,@metricDomain_ID")]
    public class MetricDomain : DataMap.ObjectBase
    {
        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Select.Parameter("@metricDomain_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Update.Parameter("@metricDomain_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Delete.Parameter("@metricDomain_ID", System.Data.SqlDbType.Int)]
        public int metricDomain_ID
        {
            get
            {
                return _metricDomain_ID;
            }

            set
            {
                if (_metricDomain_ID != value)
                {
                    _metricDomain_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _metricDomain_ID = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@metric_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@metric_ID", System.Data.SqlDbType.Int)]
        public int metric_ID
        {
            get
            {
                return _metric_ID;
            }

            set
            {
                if (_metric_ID != value)
                {
                    _metric_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _metric_ID = -1;

        [DataMap.Sql.Column("displayOrder")]
        [DataMap.Sql.Procedure.Update.Parameter("@displayOrder", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@displayOrder", System.Data.SqlDbType.Int)]
        public int order
        {
            get
            {
                return _order;
            }

            set
            {
                if (_order != value)
                {
                    _order = value;
                    onPropertyChanged();
                }
            }
        }
        private int _order = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@name", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@name", System.Data.SqlDbType.VarChar)]
        public string name
        {
            get
            {
                return _name;
            }

            set
            {
                if (_name != value)
                {
                    _name = value;
                    onPropertyChanged();
                }
            }
        }
        private string _name = null;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@value", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@value", System.Data.SqlDbType.VarChar)]
        public string value
        {
            get
            {
                return _value;
            }

            set
            {
                if (_value != value)
                {
                    _value = value;
                    onPropertyChanged();
                }
            }
        }
        private string _value = null;

        public MetricDomain()
        {
            isNew = true;
            isModified = true;
        }

        public MetricDomain(int metricDomain_ID)
        {
            this.metricDomain_ID = metricDomain_ID;
            isNew = false;
            isModified = false;
        }
    }
}
