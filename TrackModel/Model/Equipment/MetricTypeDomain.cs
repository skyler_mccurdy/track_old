﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Model.Equipment
{
    [DataMap.Sql.Procedure.Select("exec Equipment.metricTypeDomain_getByID null,@metricTypeDomain_ID")]
    [DataMap.Sql.Procedure.Update("exec Equipment.metricTypeDomain_update null,@metricTypeDomain_ID,@metricType_ID,@displayOrder,@name,@value")]
    [DataMap.Sql.Procedure.Insert("exec Equipment.metricTypeDomain_insert null,@metricType_ID,@displayOrder,@name,@value")]
    [DataMap.Sql.Procedure.Delete("exec Equipment.metricTypeDomain_delete null,@metricTypeDomain_ID")]
    public class MetricTypeDomain : DataMap.ObjectBase
    {
        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Select.Parameter("@metricTypeDomain_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Update.Parameter("@metricTypeDomain_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Delete.Parameter("@metricTypeDomain_ID", System.Data.SqlDbType.Int)]
        public int metricTypeDomain_ID
        {
            get
            {
                return _metricTypeDomain_ID;
            }

            set
            {
                if (_metricTypeDomain_ID != value)
                {
                    _metricTypeDomain_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _metricTypeDomain_ID = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@metricType_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@metricType_ID", System.Data.SqlDbType.Int)]
        public int metricType_ID
        {
            get
            {
                return _metricType_ID;
            }

            set
            {
                if (_metricType_ID != value)
                {
                    _metricType_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _metricType_ID = -1;

        [DataMap.Sql.Column("displayOrder")]
        [DataMap.Sql.Procedure.Update.Parameter("@displayOrder", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@displayOrder", System.Data.SqlDbType.Int)]
        public int order
        {
            get
            {
                return _order;
            }

            set
            {
                if (_order != value)
                {
                    _order = value;
                    onPropertyChanged();
                }
            }
        }
        private int _order = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@name", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@name", System.Data.SqlDbType.VarChar)]
        public string name
        {
            get
            {
                return _name;
            }

            set
            {
                if (_name != value)
                {
                    _name = value;
                    onPropertyChanged();
                }
            }
        }
        private string _name = null;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@value", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@value", System.Data.SqlDbType.VarChar)]
        public string value
        {
            get
            {
                return _value;
            }

            set
            {
                if (_value != value)
                {
                    _value = value;
                    onPropertyChanged();
                }
            }
        }
        private string _value = null;

        public MetricTypeDomain()
        {
            isNew = true;
            isModified = true;
        }

        public MetricTypeDomain(int metricTypeDomain_ID)
        {
            this.metricTypeDomain_ID = metricTypeDomain_ID;
            isNew = false;
            isModified = false;
        }
    }
}
