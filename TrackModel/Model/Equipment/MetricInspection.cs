﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Model.Equipment
{
    [DataMap.Sql.Procedure.Select("exec Equipment.metricInspection_getByID null,@metricInspection_ID")]
    [DataMap.Sql.Procedure.Update("exec Equipment.metricInspection_update null,@metricInspection_ID,@metric_ID,@inspectedOn,@inspectedAt,@value")]
    [DataMap.Sql.Procedure.Insert("exec Equipment.metricInspection_insert null,@metric_ID,@inspectedOn,@inspectedAt,@value")]
    [DataMap.Sql.Procedure.Delete("exec Equipment.metricInspection_delete null,@metricInspection_ID")]
    public class MetricInspection : DataMap.ObjectBase
    {
        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Select.Parameter("@metricInspection_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Update.Parameter("@metricInspection_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Delete.Parameter("@metricInspection_ID", System.Data.SqlDbType.Int)]
        public int metricInspection_ID
        {
            get
            {
                return _metricInspection_ID;
            }

            set
            {
                if (_metricInspection_ID != value)
                {
                    _metricInspection_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _metricInspection_ID = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@metric_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@metric_ID", System.Data.SqlDbType.Int)]
        public int metric_ID
        {
            get
            {
                return _metric_ID;
            }

            set
            {
                if (_metric_ID != value)
                {
                    _metric_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _metric_ID = -1;

        

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@inspectedOn", System.Data.SqlDbType.DateTime2)]
        [DataMap.Sql.Procedure.Insert.Parameter("@inspectedOn", System.Data.SqlDbType.DateTime2)]
        public DateTime inspectedOn
        {
            get
            {
                return _inspectedOn;
            }

            set
            {
                if (_inspectedOn != value)
                {
                    _inspectedOn = value;
                    onPropertyChanged();
                }
            }
        }
        private DateTime _inspectedOn;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@inspectedAt", System.Data.SqlDbType.Decimal)]
        [DataMap.Sql.Procedure.Insert.Parameter("@inspectedAt", System.Data.SqlDbType.Decimal)]
        public decimal? inspectedAt
        {
            get
            {
                return _inspectedAt;
            }

            set
            {
                if (_inspectedAt != value)
                {
                    _inspectedAt = value;
                    onPropertyChanged();
                }
            }
        }
        private decimal? _inspectedAt = null;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@value", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@value", System.Data.SqlDbType.VarChar)]
        public string value
        {
            get
            {
                return _value;
            }

            set
            {
                if (_value != value)
                {
                    _value = value;
                    onPropertyChanged();
                }
            }
        }
        private string _value = null;

        public Metric metric
        {
            get
            {
                return factory.get<Metric>(metric_ID);
            }
        }


        public MetricInspection()
        {
            isNew = true;
            isModified = true;
        }

        public MetricInspection(int metricInspection_ID)
        {
            this.metricInspection_ID = metricInspection_ID;
            isNew = false;
            isModified = true;
        }
    }
}
