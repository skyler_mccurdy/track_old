﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Track.Model.Equipment
{
    [DataMap.Sql.Procedure.Select("exec Equipment.metric_getByID null,@metric_ID")]
    [DataMap.Sql.Procedure.Update("exec Equipment.metric_update null,@metric_ID,@name,@metricType_ID,@minValue,@maxValue,@graphRange,@graphRangeType")]
    [DataMap.Sql.Procedure.Insert("exec Equipment.metric_insert null,@name,@metricType_ID,@minValue,@maxValue,@graphRange,@graphRangeType")]
    [DataMap.Sql.Procedure.Delete("exec Equipment.metric_delete null,@metric_ID")]
    public class Metric : DataMap.ObjectBase
    {
        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Select.Parameter("@metric_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Update.Parameter("@metric_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Delete.Parameter("@metric_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.SelectArray.Parameter("@metric_ID", System.Data.SqlDbType.Int)]
        public int metric_ID
        {
            get
            {
                return _metric_ID;
            }

            set
            {
                if (_metric_ID != value)
                {
                    _metric_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _metric_ID = -1;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@name", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@name", System.Data.SqlDbType.VarChar)]
        public string name
        {
            get
            {
                return _name;
            }

            set
            {
                if (_name != value)
                {
                    _name = value;
                    onPropertyChanged();
                }
            }
        }
        private string _name = null;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@metricType_ID", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@metricType_ID", System.Data.SqlDbType.Int)]
        public int metricType_ID
        {
            get
            {
                return _metricType_ID;
            }

            set
            {
                if (_metricType_ID != value)
                {
                    _metricType_ID = value;
                    onPropertyChanged();
                }
            }
        }
        private int _metricType_ID = -1;

        [DataMap.Sql.Column("minValue")]
        [DataMap.Sql.Procedure.Update.Parameter("@minValue", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@minValue", System.Data.SqlDbType.VarChar)]
        public string min
        {
            get
            {
                return _min;
            }

            set
            {
                if (_min != value)
                {
                    _min = value;
                    onPropertyChanged();
                }
            }
        }
        private string _min = null;

        [DataMap.Sql.Column("maxValue")]
        [DataMap.Sql.Procedure.Update.Parameter("@maxValue", System.Data.SqlDbType.VarChar)]
        [DataMap.Sql.Procedure.Insert.Parameter("@maxValue", System.Data.SqlDbType.VarChar)]
        public string max
        {
            get
            {
                return _max;
            }

            set
            {
                if (_max != value)
                {
                    _max = value;
                    onPropertyChanged();
                }
            }
        }
        private string _max = null;

        [DataMap.Sql.Column]
        [DataMap.Sql.Procedure.Update.Parameter("@graphRange", System.Data.SqlDbType.Decimal)]
        [DataMap.Sql.Procedure.Insert.Parameter("@graphRange", System.Data.SqlDbType.Decimal)]
        public decimal? graphRange
        {
            get
            {
                return _graphRange;
            }

            set
            {
                if (_graphRange != value)
                {
                    _graphRange = value;
                    onPropertyChanged();
                }
            }
        }
        private decimal? _graphRange = -1;

        [DataMap.Sql.Column("graphRangeType")]
        [DataMap.Sql.Procedure.Update.Parameter("@graphRangeType", System.Data.SqlDbType.Int)]
        [DataMap.Sql.Procedure.Insert.Parameter("@graphRangeType", System.Data.SqlDbType.Int)]
        public int? dbGraphRangeType
        {
            get
            {
                return _graphRangeType;
            }

            set
            {
                if (_graphRangeType != value)
                {
                    _graphRangeType = value;
                    onPropertyChanged();
                }
            }
        }
        private int? _graphRangeType = -1;

        public enum GraphRangeType
        {
            NONE,
            DAYS,
            EQUIPMENT_HOURS
        }

        public GraphRangeType graphRangeType
        {
            get
            {
                if (dbGraphRangeType == null)
                    return GraphRangeType.NONE;

                switch (dbGraphRangeType)
                {
                    default:
                    case 0:
                        return GraphRangeType.NONE;
                    case 1:
                        return GraphRangeType.DAYS;
                    case 2:
                        return GraphRangeType.EQUIPMENT_HOURS;
                }
            }

            set {
                switch (value)
                {
                    default:
                    case GraphRangeType.NONE:
                        dbGraphRangeType = 0;
                        break;
                    case GraphRangeType.DAYS:
                        dbGraphRangeType = 1;
                        break;
                    case GraphRangeType.EQUIPMENT_HOURS:
                        dbGraphRangeType = 2;
                        break;
                }
            }
        }
        
        public MetricType metricType()
        {
            return factory.get<MetricType>(metricType_ID);
        }

        [DataMap.Sql.Procedure.SelectArray("exec Equipment.metricDomain_forMetric null, @metric_ID")]
        public IEnumerable<MetricDomain> domain
        {
            get
            {
                return dataInterface.getArray<MetricDomain>();
            }
        }

        [DataMap.Sql.Procedure.SelectArray("exec Equipment.metricInspection_getForMetric null,@metric_ID")]
        public IEnumerable<MetricInspection> inspections
        {
            get
            {
                return dataInterface.getArray<MetricInspection>();
            }
        }

        public Metric()
        {
            isNew = true;
            isModified = true;
        }

        public Metric(int metric_ID)
        {
            this.metric_ID = metric_ID;
            isNew = false;
            isModified = false;
        }
    }
}
