﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Data.SqlClient;
using System.Net.Http.Formatting;

namespace Track_Rest_API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.EnableSystemDiagnosticsTracing();

            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());

            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            /*config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );*/
        }

        public static DataMap.IDataMapFactory factory
        {
            get
            {
                return DataMap.DataMapFactoryBase.factory<TrackFactory>();
            }
        }


        public class TrackFactory : DataMap.Sql.SqlFactory
        {
            private static readonly string connectionString =
                    "Data Source=.;Initial Catalog=Track;User ID=track;Password=track";

            public Track.Model.UserSecurity.Token token { get; set; }

            public TrackFactory()
            {
                connection = new SqlConnection(connectionString);
                connection.Open();

            }
        }
    }
}
