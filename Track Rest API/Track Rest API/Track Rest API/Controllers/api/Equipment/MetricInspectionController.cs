﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Track_Rest_API.Controllers.api.Equipment
{
    public class MetricInspectionController : ApiController
    {

        [Route("api/equipment/metric/{metric_ID:int}/inspection")]
        [HttpGet]
        public IEnumerable<Json> GetForMetric(int metric_ID)
        {
            return CloneIt.shallowIEnumerable<Track.Model.Equipment.MetricInspection, Json>(factory.get<Track.Model.Equipment.Metric>(metric_ID).inspections);
        }


        [Route("api/equipment/metric/{metric_ID:int}/inspection/{metricInspection_ID:int}")]
        [HttpGet]
        public Json Get(int metric_ID,int metricInspection_ID)
        {
            return CloneIt.shallow<Track.Model.Equipment.MetricInspection, Json>(factory.get<Track.Model.Equipment.MetricInspection>(metricInspection_ID));
        }

        [Route("api/equipment/metric/{metric_ID:int}/inspection")]
        [HttpPost]
        public Json Post(int metric_ID,[FromBody]Json value)
        {
            Track.Model.Equipment.MetricInspection model = factory.create<Track.Model.Equipment.MetricInspection>();
            CloneIt.shallow<Json, Track.Model.Equipment.MetricInspection>(value, ref model);
            model.metric_ID = metric_ID;
            model.inspectedOn = DateTime.Now;
            model.commit();

            return CloneIt.shallow<Track.Model.Equipment.MetricInspection, Json>(model);
        }

        [Route("api/equipment/metric/{metric_ID:int}/inspection/{metricInspection_ID:int}")]
        [HttpPut]
        public Json Put(int metric_ID, int metricInspection_ID, [FromBody]Json value)
        {
            Track.Model.Equipment.MetricInspection model = factory.get<Track.Model.Equipment.MetricInspection>(metricInspection_ID);
            CloneIt.shallow<Json, Track.Model.Equipment.MetricInspection>(value, ref model);
            model.metricInspection_ID = metricInspection_ID;
            model.metric_ID = metric_ID;
            model.inspectedOn = DateTime.Now;
            model.commit();

            return CloneIt.shallow<Track.Model.Equipment.MetricInspection, Json>(model);
        }
        /*
        [Route("api/equipment/metric/{metric_ID:int}/inspection/{metricInspection_ID:int}")]
        public string Delete(int metric_ID, int metricInspection_ID)
        {
            Track.Model.Equipment.MetricInspection model = factory.get<Track.Model.Equipment.MetricInspection>(metricInspection_ID);
            model.delete();

            return "Ok";
        }
        */
        public class Json
        {
            public int metricInspection_ID { get; set; }
            public int metric_ID { get; set; }
            public DateTime? inspectedOn { get; set; }
            public decimal? inspectedAt { get; set; }
            
            public string value { get; set; }
        }


        public DataMap.IDataMapFactory factory
        {
            get
            {
                return WebApiConfig.factory;
            }
        }
    }
}
