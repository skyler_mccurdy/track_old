﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Track_Rest_API.Controllers.api.Equipment
{
    public class MetricTypeController : ApiController
    {


        [Route("api/equipment/metricType/{id:int}")]
        [HttpGet]
        public Json Get(int id)
        {
            return CloneIt.shallow<Track.Model.Equipment.MetricType, Json>(factory.get<Track.Model.Equipment.MetricType>(id));
        }
        /*
        [Route("api/equipment/metricType")]
        [HttpPost]
        public Json Post([FromBody]Json value)
        {
            Track.Model.Equipment.MetricType model = factory.create<Track.Model.Equipment.MetricType>();
            CloneIt.shallow<Json, Track.Model.Equipment.MetricType>(value, ref model);
            model.commit();

            return CloneIt.shallow<Track.Model.Equipment.MetricType, Json>(model);
        }

        [Route("api/equipment/metricType/{id:int}")]
        public Json Put(int id, [FromBody]Json value)
        {
            Track.Model.Equipment.MetricType model = factory.get<Track.Model.Equipment.MetricType>(id);
            CloneIt.shallow<Json, Track.Model.Equipment.MetricType>(value, ref model);
            model.metricType_ID = id;
            model.commit();

            return CloneIt.shallow<Track.Model.Equipment.MetricType, Json>(model);
        }

        [Route("api/equipment/metricType/{id:int}")]
        public string Delete(int id)
        {
            Track.Model.Equipment.MetricType model = factory.get<Track.Model.Equipment.MetricType>(id);
            model.delete();

            return "Ok";
        }*/

        public class Json
        {
            public int metricType_ID { get; set; }
            public string name { get; set; }
            public string min { get; set; }
            public string max { get; set; }
            public bool isDiscreet { get; set; }
            public decimal graphRange { get; set; }
            public int graphRangeType { get; set; }
        }


        public DataMap.IDataMapFactory factory
        {
            get
            {
                return WebApiConfig.factory;
            }
        }
    }
}
