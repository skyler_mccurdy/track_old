﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Track_Rest_API.Controllers.api.Equipment
{
    public class MetricDomainController : ApiController
    {

        [Route("api/equipment/metric/{metric_ID:int}/metricDomain")]
        [HttpGet]
        public IEnumerable<Json> GetForMetric(int metric_ID)
        {
            return CloneIt.shallowIEnumerable<Track.Model.Equipment.MetricDomain, Json>(factory.get<Track.Model.Equipment.Metric>(metric_ID).domain);
        }


        [Route("api/equipment/metric/{metric_ID:int}/domain/{metricDomain_ID:int}")]
        [HttpGet]
        public Json Get(int metric_ID,int metricDomain_ID)
        {
            return CloneIt.shallow<Track.Model.Equipment.MetricDomain, Json>(factory.get<Track.Model.Equipment.MetricDomain>(metricDomain_ID));
        }
        /*
        [Route("api/equipment/metric/{metric_ID:int}/domain")]
        [HttpPost]
        public Json Post(int metric_ID,[FromBody]Json value)
        {
            Track.Model.Equipment.MetricDomain model = factory.create<Track.Model.Equipment.MetricDomain>();
            CloneIt.shallow<Json, Track.Model.Equipment.MetricDomain>(value, ref model);
            model.commit();

            return CloneIt.shallow<Track.Model.Equipment.MetricDomain, Json>(model);
        }

        [Route("api/equipment/metric/{metric_ID:int}/domain/{metricDomain_ID:int}")]
        public Json Put(int metric_ID,int metricDomain_ID, [FromBody]Json value)
        {
            Track.Model.Equipment.MetricDomain model = factory.get<Track.Model.Equipment.MetricDomain>(metricDomain_ID);
            CloneIt.shallow<Json, Track.Model.Equipment.MetricDomain>(value, ref model);
            model.metricDomain_ID = metricDomain_ID;
            model.metric_ID = metric_ID;
            model.commit();

            return CloneIt.shallow<Track.Model.Equipment.MetricDomain, Json>(model);
        }

        [Route("api/equipment/metric/{metric_ID:int}/domain/{metricDomain_ID:int}")]
        public string Delete(int metric_ID,int metricDomain_ID)
        {
            Track.Model.Equipment.MetricDomain model = factory.get<Track.Model.Equipment.MetricDomain>(metricDomain_ID);
            model.delete();

            return "Ok";
        }
        */
        public class Json
        {
            public int metricDomain_ID { get; set; }
            public int metric_ID { get; set; }
            public int order { get; set; }
            public string name { get; set; }
            public string value { get; set; }
        }


        public DataMap.IDataMapFactory factory
        {
            get
            {
                return WebApiConfig.factory;
            }
        }
    }
}
