﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Track_Rest_API.Controllers.api.Equipment
{
    public class MetricController : ApiController
    {


        [Route("api/equipment/metric/{id:int}")]
        [HttpGet]
        public Json Get(int id)
        {
            return CloneIt.shallow<Track.Model.Equipment.Metric, Json>(factory.get<Track.Model.Equipment.Metric>(id));
        }
        /*
        [Route("api/equipment/metric")]
        [HttpPost]
        public Json Post([FromBody]Json value)
        {
            Track.Model.Equipment.Metric model = factory.create<Track.Model.Equipment.Metric>();
            CloneIt.shallow<Json, Track.Model.Equipment.Metric>(value, ref model);
            model.commit();

            return CloneIt.shallow<Track.Model.Equipment.Metric, Json>(model);
        }

        [Route("api/equipment/metric/{id:int}")]
        public Json Put(int id, [FromBody]Json value)
        {
            Track.Model.Equipment.Metric model = factory.get<Track.Model.Equipment.Metric>(id);
            CloneIt.shallow<Json, Track.Model.Equipment.Metric>(value, ref model);
            model.metric_ID = id;
            model.commit();

            return CloneIt.shallow<Track.Model.Equipment.Metric, Json>(model);
        }

        [Route("api/equipment/metric/{id:int}")]
        public string Delete(int id)
        {
            Track.Model.Equipment.Metric model = factory.get<Track.Model.Equipment.Metric>(id);
            model.delete();

            return "Ok";
        }*/

        public class Json
        {
            public int metric_ID { get; set; }
            public string name { get; set; }
            public int metricType_ID { get; set; }
            public string min { get; set; }
            public string max { get; set; }
            public decimal graphRange { get; set; }
            public int graphRangeType { get; set; }
        }


        public DataMap.IDataMapFactory factory
        {
            get
            {
                return WebApiConfig.factory;
            }
        }
    }
}
