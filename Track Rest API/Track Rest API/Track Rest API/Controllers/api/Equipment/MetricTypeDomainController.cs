﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Track_Rest_API.Controllers.api.Equipment
{
    public class MetricTypeDomainController : ApiController
    {


        [Route("api/equipment/metricType/{metricType_ID:int}/domain/{metricTypeDomain_ID:int}")]
        [HttpGet]
        public Json Get(int metricType_ID, int metricTypeDomain_ID)
        {
            return CloneIt.shallow<Track.Model.Equipment.MetricTypeDomain, Json>(factory.get<Track.Model.Equipment.MetricTypeDomain>(metricTypeDomain_ID));
        }
        /*
        [Route("api/equipment/metricType/{metricType_ID:int}/domain")]
        [HttpPost]
        public Json Post(int metricType_ID, [FromBody]Json value)
        {
            Track.Model.Equipment.MetricTypeDomain model = factory.create<Track.Model.Equipment.MetricTypeDomain>();
            CloneIt.shallow<Json, Track.Model.Equipment.MetricTypeDomain>(value, ref model);
            model.commit();

            return CloneIt.shallow<Track.Model.Equipment.MetricTypeDomain, Json>(model);
        }

        [Route("api/equipment/metricType/{metricType_ID:int}/domain/{metricTypeDomain_ID:int}")]
        public Json Put(int metricType_ID, int metricTypeDomain_ID, [FromBody]Json value)
        {
            Track.Model.Equipment.MetricTypeDomain model = factory.get<Track.Model.Equipment.MetricTypeDomain>(metricTypeDomain_ID);
            CloneIt.shallow<Json, Track.Model.Equipment.MetricTypeDomain>(value, ref model);
            model.metricTypeDomain_ID = metricTypeDomain_ID;
            model.metricType_ID = metricType_ID;
            model.commit();

            return CloneIt.shallow<Track.Model.Equipment.MetricTypeDomain, Json>(model);
        }

        [Route("api/equipment/metricType/{metricType_ID:int}/domain/{metricTypeDomain_ID:int}")]
        public string Delete(int metricType_ID, int metricTypeDomain_ID)
        {
            Track.Model.Equipment.MetricTypeDomain model = factory.get<Track.Model.Equipment.MetricTypeDomain>(metricTypeDomain_ID);
            model.delete();

            return "Ok";
        }*/

        public class Json
        {
            public int metricTypeDomain_ID { get; set; }
            public int metricType_ID { get; set; }
            public int order { get; set; }
            public string name { get; set; }
            public string value { get; set; }
        }


        public DataMap.IDataMapFactory factory
        {
            get
            {
                return WebApiConfig.factory;
            }
        }
    }
}
