﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Track_Rest_API.Controllers.api.PPM
{
    public class PPMMetricController : ApiController
    {


        [Route("api/ppm/metricCategory/{category_ID:int}/metrics")]
        [HttpGet]
        public IEnumerable<Json> GetForCategory(int category_ID)
        {
            return CloneIt.shallowIEnumerable<Track.Model.PPM.Metric, Json>(factory.get<Track.Model.PPM.MetricCategory>(category_ID).metrics);
        }

        [Route("api/ppm/metric/{id:int}")]
        [HttpGet]
        public Json Get(int id)
        {
            return CloneIt.shallow<Track.Model.PPM.Metric, Json>(factory.get<Track.Model.PPM.Metric>(id));
        }
        /*
        [Route("api/ppm/metric")]
        [HttpPost]
        public Json Post([FromBody]Json value)
        {
            Track.Model.PPM.Metric model = factory.create<Track.Model.PPM.Metric>();
            CloneIt.shallow<Json, Track.Model.PPM.Metric>(value, ref model);
            model.commit();

            return CloneIt.shallow<Track.Model.PPM.Metric, Json>(model);
        }

        [Route("api/ppm/metric/{id:int}")]
        public Json Put(int id, [FromBody]Json value)
        {
            Track.Model.PPM.Metric model = factory.get<Track.Model.PPM.Metric>(id);
            CloneIt.shallow<Json, Track.Model.PPM.Metric>(value, ref model);
            model.metric_ID = id;
            model.commit();

            return CloneIt.shallow<Track.Model.PPM.Metric, Json>(model);
        }

        [Route("api/ppm/metric/{id:int}")]
        public string Delete(int id)
        {
            Track.Model.PPM.Metric model = factory.get<Track.Model.PPM.Metric>(id);
            model.delete();

            return "Ok";
        }*/

        public class Json
        {
            public int metric_ID { get; set; }
            public int equipmentMetric_ID { get; set; }
            public int category_ID { get; set; }
            public int order { get; set; }
            public string note { get; set; }

        }


        public DataMap.IDataMapFactory factory
        {
            get
            {
                return WebApiConfig.factory;
            }
        }
    }
}
