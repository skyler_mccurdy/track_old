﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Track_Rest_API.Controllers.api.PPM
{
    public class PPMMetricCategoryController : ApiController
    {

        [Route("api/ppm/{ppm_ID:int}/metricCategories")]
        [HttpGet]
        public IEnumerable<Json> GetForPPM(int ppm_ID)
        {
            return CloneIt.shallowIEnumerable<Track.Model.PPM.MetricCategory, Json>(factory.get<Track.Model.PPM.PPM>(ppm_ID).categories);
        }

        [Route("api/ppm/metricCategory/{category_ID:int}/categories")]
        [HttpGet]
        public IEnumerable<Json> GetForCategory(int category_ID)
        {
            return CloneIt.shallowIEnumerable<Track.Model.PPM.MetricCategory, Json>(factory.get<Track.Model.PPM.MetricCategory>(category_ID).categories);
        }

        [Route("api/ppm/metricCategory/{id:int}")]
        [HttpGet]
        public Json Get(int id)
        {
            return CloneIt.shallow<Track.Model.PPM.MetricCategory, Json>(factory.get<Track.Model.PPM.MetricCategory>(id));
        }
        /*
        [Route("api/ppm/metricCategory")]
        [HttpPost]
        public Json Post([FromBody]Json value)
        {
            Track.Model.PPM.MetricCategory model = factory.create<Track.Model.PPM.MetricCategory>();
            CloneIt.shallow<Json, Track.Model.PPM.MetricCategory>(value, ref model);
            model.commit();

            return CloneIt.shallow<Track.Model.PPM.MetricCategory, Json>(model);
        }

        [Route("api/ppm/metricCategory/{id:int}")]
        public Json Put(int id, [FromBody]Json value)
        {
            Track.Model.PPM.MetricCategory model = factory.get<Track.Model.PPM.MetricCategory>(id);
            CloneIt.shallow<Json, Track.Model.PPM.MetricCategory>(value, ref model);
            model.metricCategory_ID = id;
            model.commit();

            return CloneIt.shallow<Track.Model.PPM.MetricCategory, Json>(model);
        }

        [Route("api/ppm/metricCategory/{id:int}")]
        public string Delete(int id)
        {
            Track.Model.PPM.MetricCategory model = factory.get<Track.Model.PPM.MetricCategory>(id);
            model.delete();

            return "Ok";
        }*/

        public class Json
        {
            public int metricCategory_ID { get; set; }
            public int ppm_ID { get; set; }
            public int? parent_ID { get; set; }
            public int order { get; set; }
            public string name { get; set; }
            public string note { get; set; }

        }


        public DataMap.IDataMapFactory factory
        {
            get
            {
                return WebApiConfig.factory;
            }
        }
    }
}
