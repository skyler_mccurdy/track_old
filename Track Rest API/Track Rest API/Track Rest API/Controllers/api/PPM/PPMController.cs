﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Track_Rest_API.Controllers.api.PPM
{
    public class PPMController : ApiController
    {
        public DataMap.IDataMapFactory factory
        {
            get
            {
                return WebApiConfig.factory;
            }
        }

        public class Json
        {
            public int ppm_ID { get; set; }

            public string name { get; set; }

            public string head { get; set; }

            public string mp2_PPM { get; set; }

        }

        [Route("api/ppm")]
        [HttpGet]
        public IEnumerable<Json> Get()
        {
            return CloneIt.shallowIEnumerable<Track.Model.PPM.PPM,Json>(Track.Model.PPM.PPM.getAll(factory));
        }

        [Route("api/ppm/{id:int}")]
        [HttpGet]
        public Json Get(int id)
        {
            return CloneIt.shallow<Track.Model.PPM.PPM, Json>(factory.get<Track.Model.PPM.PPM>(id));
        }
        /*

        [Route("api/ppm")]
        public Json Post([FromBody]Json value)
        {
            Track.Model.PPM.PPM ppm = factory.create<Track.Model.PPM.PPM>();
            CloneIt.shallow<Json,Track.Model.PPM.PPM>(value,ref ppm);
            ppm.commit();

            return CloneIt.shallow<Track.Model.PPM.PPM, Json>(ppm);
        }

        [Route("api/ppm/{id:int}")]
        public Json Put(int id, [FromBody]Json value)
        {
            Track.Model.PPM.PPM ppm = factory.get<Track.Model.PPM.PPM>(id);
            CloneIt.shallow<Json, Track.Model.PPM.PPM>(value, ref ppm);
            ppm.ppm_ID = id;
            ppm.commit();

            return CloneIt.shallow<Track.Model.PPM.PPM, Json>(ppm);
        }

        [Route("api/ppm/{id:int}")]
        public string Delete(int id)
        {
            Track.Model.PPM.PPM ppm = factory.get<Track.Model.PPM.PPM>(id);
            ppm.delete();

            return "Ok";
        }
         */
    }
}
