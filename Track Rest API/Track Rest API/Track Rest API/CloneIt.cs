﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace Track_Rest_API
{
    public class CloneIt
    {

        public static void shallow<I, O>(I input,ref O output)
        {
            if (input == null || output == null)
                return;

            foreach (PropertyInfo out_prop in typeof(O).GetProperties())
            {
                PropertyInfo in_prop = input.GetType().GetProperty(out_prop.Name);
                if (in_prop != null)
                {
                    out_prop.SetValue(output, in_prop.GetValue(input));
                }
            }

        }

        public static O shallow<I, O>(I input)
        {
            if (input == null)
                return default(O);

            O output = Activator.CreateInstance<O>();

            foreach(PropertyInfo out_prop in typeof(O).GetProperties())
            {
                PropertyInfo in_prop = input.GetType().GetProperty(out_prop.Name);
                if (in_prop != null)
                {
                    out_prop.SetValue(output, in_prop.GetValue(input));
                }
            }

            return output;
        }

        public static O[] shallowArray<I, O>(I[] input)
        {
            O[] output = new O[input.Count()];

            for (int i = 0; i < input.Count(); i++)
            {
                output[i] = shallow<I, O>(input[i]);
            }

            return output;
        }

        public static IEnumerable<O> shallowIEnumerable<I, O>(IEnumerable<I> input)
        {
            List<O> output = new List<O>();

            foreach (I tmp in input)
            {
                output.Add(shallow<I, O>(tmp));
            }

            return output;
        }
    }
}