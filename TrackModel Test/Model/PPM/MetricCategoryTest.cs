﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TrackModel_Test.Model.PPM
{
    [TestClass]
    public class MetricCategoryTest
    {
        [TestMethod]
        public void PPM_MetricCategory_Get()
        {

            int category_ID = 4;
            int ppm_ID = 1;
            int? parent_ID = 1;
            string name = "Cat 1.1";
            string note = "cat 1.1 notes";

            DataMap.IDataMapFactory factory = DataMap.DataMapFactoryBase.factory<DBTests.TestFactory>();
            Track.Model.PPM.MetricCategory model = factory.get<Track.Model.PPM.MetricCategory>(category_ID);

            Assert.IsNotNull(model);
            Assert.AreEqual(category_ID,model.metricCategory_ID);
            Assert.AreEqual(ppm_ID,model.ppm_ID);
            Assert.AreEqual(parent_ID,model.parent_ID);
            Assert.AreEqual(name,model.name);
            Assert.AreEqual(note,model.note);



        }
    }
}
