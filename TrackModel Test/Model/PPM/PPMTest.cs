﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TrackModel_Test.Model.PPM
{
    [TestClass]
    public class PPMTest
    {
       
        [TestMethod]
        public void ppm_getById()
        {
            int ppm_ID = 1;
            string name = "test PPM";
            string head = "test text";
            string mp2_PPM = "test ppm";
            

            DBTests.TestFactory factory = DataMap.DataMapFactoryBase.factory<DBTests.TestFactory>();

            Track.Model.PPM.PPM ppm = factory.get<Track.Model.PPM.PPM>(ppm_ID);

            Assert.IsNotNull(ppm);
            Assert.AreEqual(ppm_ID, ppm.ppm_ID);
            Assert.AreEqual(name, ppm.name);
            Assert.AreEqual(head, ppm.head);
            Assert.AreEqual(mp2_PPM, ppm.mp2_PPM);

        }
       

        public PPMTest()
        {
            DBTests.setup();
        }
    }
}
