﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace TrackModel_Test
{
    class DBTests
    {

        
        private static readonly string connectionString =
                "Data Source=WIN81;Initial Catalog=Track_Test;Integrated Security=SSPI;MultipleActiveResultSets=True";

        public static void setup()
        {
            LogIt.LogIt.targets.Clear();
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();
            //Track.Model.Object.connection = con;
            //Track.Model.UserSecurity.Token.authenticate("win81\\skyler","test");
        }


        public class TestFactory : DataMap.Sql.SqlFactory
        {
            public TestFactory()
            {
                connection = new SqlConnection(connectionString);
                connection.Open();

                //token = Track.Model.UserSecurity.Token.authenticate("win81\\Skyler", "testMachine");
            }

            public Track.Model.UserSecurity.Token token { get; set; }
        }
        
    }
}
