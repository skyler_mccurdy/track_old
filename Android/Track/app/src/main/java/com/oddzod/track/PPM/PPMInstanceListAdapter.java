package com.oddzod.track.PPM;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.oddzod.track.R;
import com.oddzod.track.model.ppm.PPM;

public class PPMInstanceListAdapter extends ArrayAdapter<PPM>{

    private int layout;
    private Context context;

    public PPMInstanceListAdapter(Context context, int layout,List<PPM> items) {
        super(context, layout,items);

        this.layout=layout;
        this.context=context;
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(layout,parent,false);

        ((TextView)view.findViewById(R.id.txvPPMTitle)).setText(getItem(position).getName());


        return view;

    }
}

