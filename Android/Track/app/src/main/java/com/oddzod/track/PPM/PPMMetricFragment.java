package com.oddzod.track.PPM;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.*;

import com.oddzod.dataMap.DataMap;
import com.oddzod.track.R;
import com.oddzod.track.TrackFactory;
import com.oddzod.track.model.equipment.MetricDomain;
import com.oddzod.track.model.equipment.MetricInspection;
import com.oddzod.track.model.equipment.MetricType;
import com.oddzod.track.model.ppm.Metric;
import com.oddzod.track.model.ppm.MetricCategory;
import com.oddzod.track.model.ppm.PPM;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


public class PPMMetricFragment extends Fragment {

    @InjectView(R.id.lstMetrics)
    ListView metricList;

    private OnFragmentInteractionListener mListener;

    private com.oddzod.track.model.ppm.PPM ppm;

    public static PPMMetricFragment newInstance(String param1, String param2) {
        PPMMetricFragment fragment = new PPMMetricFragment();

        return fragment;
    }
    public PPMMetricFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ppmmetric, container, false);

        ButterKnife.inject(this,view);

        //updateUI();

        return view;
    }

    private void updateUI(){
        if(cats != null) {
            PPMMetricListAdapter adapter;
            adapter = new PPMMetricListAdapter(getActivity(), R.layout.list_ppm_metric_category, getCategoryList());
            metricList.setAdapter(adapter);
            metricList.setDivider(null);
        }else{
            load();
        }
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private class Cat{
        public MetricCategory category = null;

        public Metric[] metrics = null;
        public List<com.oddzod.track.model.equipment.Metric> equipmentMetrics  = null;
        public List<MetricType> metricTypes = null;
        public List<MetricDomain[]> metricDomains = null;
        public List<MetricInspection> metricInspections = null;

        public List<Cat> categories = null;
    }

    private List<Cat> cats = null;




    private void addCategory(String prefix,Cat category,List<PPMMetricItem> list){

        PPMMetricItem.PPMMetricItemCategory cat = new PPMMetricItem.PPMMetricItemCategory();
        cat.name = prefix + " " + category.category.getName();
        cat.notes = category.category.getNote();
        list.add(cat);

        for(int i=0;i<category.metrics.length;i++){

            PPMMetricItem item = null;

            if(category.metricTypes.get(i).getIsDiscreet()){
                item = new PPMMetricItem.PPMMetricItemList();
            }else{
                item = new PPMMetricItem.PPMMetricItemText();
            }

            item.name = prefix + "." + (i+1) +" "+category.equipmentMetrics.get(i).getName();


            item.metric = category.equipmentMetrics.get(i);
            item.type = category.metricTypes.get(i);
            item.domain = category.metricDomains.get(i);
            item.inspection = category.metricInspections.get(i);


            list.add(item);
        }

        if(category.categories != null) {
            for (int i = 0; i < category.categories.size(); i++) {
                addCategory(prefix + "." + (i + 1), category.categories.get(i), list);
            }
        }


    }

    public ArrayList<PPMMetricItem> getCategoryList(){
        ArrayList<PPMMetricItem> list = new ArrayList<PPMMetricItem>();


        if(cats != null) {
            for (int i = 0; i < cats.size(); i++) {
                addCategory(Integer.toString(i + 1), cats.get(i), list);
            }
        }


        return list;
    }

    public void setPPM(PPM PPM) {
        this.ppm = PPM;
        updateUI();
    }


    public interface OnFragmentInteractionListener {

    }


    public void load(){

        class Load extends AsyncTask<Object,Integer,Long> {


            public Load(){

            }

            @Override
            protected Long doInBackground(Object... objs) {

                try {

                    MetricCategory[] categories = ppm.getMetricCategories();

                    cats = new ArrayList<Cat>();

                    for(int i=0;i<categories.length;i++){
                        Cat cat = new Cat();
                        cat.category = categories[i];
                        cat.metrics = cat.category.getMetrics();

                        cat.metricTypes = new ArrayList<MetricType>();
                        cat.equipmentMetrics = new ArrayList<com.oddzod.track.model.equipment.Metric>();
                        cat.metricDomains = new ArrayList<MetricDomain[]>();
                        cat.metricInspections = new ArrayList<MetricInspection>();

                        for(int m =0;m<cat.metrics.length;m++){
                            cat.equipmentMetrics.add(cat.metrics[m].getEquipmentMetric());
                            cat.metricTypes.add(cat.equipmentMetrics.get(m).getMetricType());
                            if(cat.metricTypes.get(m).getIsDiscreet()){
                                cat.metricDomains.add(cat.equipmentMetrics.get(m).getMetricDomain());
                            }else{
                                cat.metricDomains.add(null);
                            }

                            MetricInspection[] inspections = cat.equipmentMetrics.get(m).getInspections();
                            MetricInspection insp = null;

                            if(inspections.length>0){
                                long lastInspection = inspections[inspections.length-1].getInspectedOn().getTime();
                                long now = new Date().getTime();
                                long diff = (now - lastInspection) / 1000 / 60 / 60;

                                if(diff < 24){ //hours
                                    insp = inspections[inspections.length-1];
                                }
                            }
                            if(insp == null){
                                insp = cat.equipmentMetrics.get(m).createInspection();
                            }

                            cat.metricInspections.add(insp);

                        }

                        cats.add(cat);
                    }


                } catch (Exception e) {
                    return new Long(-1);
                }

                return new Long(0);
            }

            @Override
            protected void onPostExecute(Long result){


                if(result == 0) {

                    updateUI();



                }else{
                    Toast.makeText(getActivity().getApplicationContext(), "Error reading metric list .", Toast.LENGTH_LONG).show();
                }

                loading.set(false);
            }
        }

        try {
            if(!loading.getAndSet(true)) {
                Load load = new Load();
                load.execute(this);
            }

        }catch(Exception e){
            Toast.makeText(getActivity().getApplicationContext(),"Error reading metric list.",Toast.LENGTH_LONG).show();
        }
    }

    private AtomicBoolean loading = new AtomicBoolean(false);
}
