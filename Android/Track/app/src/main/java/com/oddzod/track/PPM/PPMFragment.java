package com.oddzod.track.PPM;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

import butterknife.*;

import com.oddzod.track.R;
import com.oddzod.track.model.ppm.PPM;


public class PPMFragment extends Fragment {


    private PPMHeadFragment headFragment = null;
    private PPMMetricFragment metricFragment = null;

    @InjectView(R.id.txvPPMTitle)
    TextView txvTitle;

    @InjectView(R.id.tabHead)
    TextView txvTabHead;

    @InjectView(R.id.tabMetrics)
    TextView txvTabMetrics;

    @InjectView(R.id.tabFeedback)
    TextView txvTabFeedback;

    @OnClick(R.id.tabHead)
    public void tabHeadOnClick() {
        if (headFragment == null){
            headFragment = new PPMHeadFragment();
            headFragment.setPPM(ppm);
        }
        getChildFragmentManager().beginTransaction()
                .replace(R.id.ppmContainer,headFragment)
                .commit();

        txvTabHead.setBackgroundColor(getResources().getColor(R.color.ppmFragment_Tab_Selected_BackgroundColor));
        txvTabMetrics.setBackgroundColor(getResources().getColor(R.color.ppmFragment_Tab_BackgroundColor));
        txvTabFeedback.setBackgroundColor(getResources().getColor(R.color.ppmFragment_Tab_BackgroundColor));
    }

    @OnClick(R.id.tabMetrics)
    public void tabMetricsOnClick(){
        if(metricFragment == null){
            metricFragment = new PPMMetricFragment();
            metricFragment.setPPM(ppm);
        }
        getChildFragmentManager().beginTransaction()
                .replace(R.id.ppmContainer,metricFragment)
                .commit();

        txvTabHead.setBackgroundColor(getResources().getColor(R.color.ppmFragment_Tab_BackgroundColor));
        txvTabMetrics.setBackgroundColor(getResources().getColor(R.color.ppmFragment_Tab_Selected_BackgroundColor));
        txvTabFeedback.setBackgroundColor(getResources().getColor(R.color.ppmFragment_Tab_BackgroundColor));
    }

    @OnClick(R.id.tabFeedback)
    public void tabFeedbackOnClick(){
        getChildFragmentManager().beginTransaction()
                .replace(R.id.ppmContainer,new PPMFeedbackFragment())
                .commit();

        txvTabHead.setBackgroundColor(getResources().getColor(R.color.ppmFragment_Tab_BackgroundColor));
        txvTabMetrics.setBackgroundColor(getResources().getColor(R.color.ppmFragment_Tab_BackgroundColor));
        txvTabFeedback.setBackgroundColor(getResources().getColor(R.color.ppmFragment_Tab_Selected_BackgroundColor));
    }



    public static PPMFragment newInstance() {
        PPMFragment fragment = new PPMFragment();

        return fragment;
    }
    public PPMFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ppm, container, false);

        ButterKnife.inject(this, view);

        updateUI();

        tabHeadOnClick();

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        updateUI();

        if(headFragment != null){
            headFragment.setPPM(ppm);
        }
        if(metricFragment != null){
            //metricFragment.setPPM(ppm);
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


    private void updateUI(){
        if(ppm != null){

            if(txvTitle != null){
                txvTitle.setText(ppm.getName());
            }

        }else{


            if(txvTitle != null){
                txvTitle.setText(ppm.getName());
            }
        }
    }

    private OnFragmentInteractionListener listener;



    public interface OnFragmentInteractionListener {

    }

    private PPM ppm;

    public void setPPM(PPM ppm){
        this.ppm=ppm;

        if(headFragment != null){
            headFragment.setPPM(ppm);
        }
        if(metricFragment != null){
            metricFragment.setPPM(ppm);
        }
    }


}
