package com.oddzod.track.PPM;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.*;
import com.oddzod.track.R;

public class PPMFeedbackFragment extends Fragment {

    @OnClick(R.id.btnComplete)
    void completeOnClick(){
        mListener.openHome();
    }


    private OnFragmentInteractionListener mListener;


    public static PPMFeedbackFragment newInstance(String param1, String param2) {
        PPMFeedbackFragment fragment = new PPMFeedbackFragment();

        return fragment;
    }
    public PPMFeedbackFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ppmfeedback, container, false);

        ButterKnife.inject(this,view);

        return view;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {

        public void openHome();
    }

}
