package com.oddzod.track.PPM;

import android.content.Context;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.oddzod.track.R;
import com.oddzod.track.model.equipment.Metric;
import com.oddzod.track.model.equipment.MetricDomain;
import com.oddzod.track.model.equipment.MetricInspection;
import com.oddzod.track.model.equipment.MetricType;
import com.oddzod.track.model.ppm.MetricCategory;

public class PPMMetricItem {

    public String name = "Name";
    public Metric metric = null;
    public MetricInspection inspection = null;
    public MetricType type = null;
    public MetricDomain[] domain = null;


    public View getView(int position, View convertView, ViewGroup parent,Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;


        view = inflater.inflate(R.layout.list_ppm_metric_item, parent, false);

        TextView txvName = (TextView) view.findViewById(R.id.txvName);
        txvName.setText(name);


        return view;
    }


    public static class PPMMetricItemCategory extends PPMMetricItem{

        public String notes;

        @Override
        public View getView(int position, View convertView, ViewGroup parent,Context context) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view;

            view = inflater.inflate(R.layout.list_ppm_metric_category, parent, false);

            TextView txvName = (TextView) view.findViewById(R.id.txvName);
            txvName.setText(name);

            TextView txvNotes = (TextView) view.findViewById(R.id.txvNote);
            txvNotes.setText(notes);

            return view;
        }
    }

    public static class PPMMetricItemNumber extends PPMMetricItem{

        public String value;




        @Override
        public View getView(int position, View convertView, ViewGroup parent,Context context) {

            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view;

            view = inflater.inflate(R.layout.list_ppm_metric_item_number, parent, false);

            TextView txvName = (TextView) view.findViewById(R.id.txvName);
            txvName.setText(name);

            return view;
        }
    }

    public static class PPMMetricItemText extends PPMMetricItem{

        public String value;
        EditText edtText = null;
        private Context context = null;

        @Override
        public View getView(int position, View convertView, ViewGroup parent,Context context) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view;
            this.context = context;

            view = inflater.inflate(R.layout.list_ppm_metric_item_text, parent, false);

            TextView txvName = (TextView) view.findViewById(R.id.txvName);
            txvName.setText(name);

            edtText = (EditText) view.findViewById(R.id.edtValue);
            if(inspection!=null && inspection.getValue() != null){
                edtText.setText(inspection.getValue());
            }else{
                edtText.setText("");
            }

           edtText.addTextChangedListener(new TextWatcher() {
               @Override
               public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

               }

               @Override
               public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

               }

               @Override
               public void afterTextChanged(Editable editable) {
                   save(editable.toString());

               }
           });


            return view;
        }



        public void save(String value){

            class Save extends AsyncTask<String,Integer,Long> {


                public Save(){

                }

                @Override
                protected Long doInBackground(String... strs) {

                    try {

                        inspection.setValue(strs[0]);
                        inspection.commit();

                    } catch (Exception e) {
                        return new Long(-1);
                    }

                    return new Long(0);
                }

                @Override
                protected void onPostExecute(Long result){


                }
            }

            try {

                Save save = new Save();
                save.execute(value);

            }catch(Exception e){

            }
        }




    }

    public static class PPMMetricItemCheckBox extends PPMMetricItem{

        public String value;

        @Override
        public View getView(int position, View convertView, ViewGroup parent,Context context) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view;

            view = inflater.inflate(R.layout.list_ppm_metric_item_checkbox, parent, false);

            TextView txvName = (TextView) view.findViewById(R.id.txvName);
            txvName.setText(name);


            return view;
        }
    }

    public static class PPMMetricItemList extends PPMMetricItem{

        public String value;
        private Context context = null;

        @Override
        public View getView(int position, View convertView, ViewGroup parent,Context context) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view;
            this.context=context;

            view = inflater.inflate(R.layout.list_ppm_metric_item_list, parent, false);

            TextView txvName = (TextView) view.findViewById(R.id.txvName);
            txvName.setText(name);

            Spinner sprValue = (Spinner) view.findViewById(R.id.sprValue);


            ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,android.R.layout.simple_list_item_1);
            if(domain != null){
                for(int i=0;i<domain.length;i++){
                    adapter.add(domain[i].getName());
                }
            }

            sprValue.setAdapter(adapter);

            int p = 0;
            if(inspection != null ){
                for(int i=0;i<domain.length;i++){
                    if(domain[i].getValue().equals(inspection.getValue())){
                        p=i;
                    }
                }
            }
            sprValue.setSelection(p);

            sprValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    save(domain[i].getValue());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            return view;
        }



        public void save(String value){

            class Save extends AsyncTask<String,Integer,Long> {


                public Save(){

                }

                @Override
                protected Long doInBackground(String... strs) {

                    try {

                        inspection.setValue(strs[0]);
                        inspection.commit();

                    } catch (Exception e) {
                        return new Long(-1);
                    }

                    return new Long(0);
                }

                @Override
                protected void onPostExecute(Long result){


                }
            }

            try {

                Save save = new Save();
                save.execute(value);

            }catch(Exception e){

            }
        }
    }

}
