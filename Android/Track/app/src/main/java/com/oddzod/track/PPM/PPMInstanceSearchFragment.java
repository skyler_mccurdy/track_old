package com.oddzod.track.PPM;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.*;

import com.oddzod.dataMap.DataMap;
import com.oddzod.track.R;
import com.oddzod.track.TrackFactory;
import com.oddzod.track.model.ppm.PPM;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;


public class PPMInstanceSearchFragment extends Fragment {

    PPM[] ppms = null;

    @InjectView(R.id.lstPPMInstance)
    ListView ppmList;

    @OnItemClick(R.id.lstPPMInstance)
    void onItemSelected(int position){
        mListener.openPPMInstance(ppms[position]);
    }

    public PPMInstanceSearchFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_ppminstance_search, container, false);

        ButterKnife.inject(this,view);


        load();


        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }


        load();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        public void openPPMInstance(PPM ppm);

    }

    private OnFragmentInteractionListener mListener;

    public static PPMInstanceSearchFragment newInstance() {
        PPMInstanceSearchFragment fragment = new PPMInstanceSearchFragment();

        return fragment;
    }












    public void load(){

        class Load extends AsyncTask<Object,Integer,Long> {


            public Load(){



            }

            @Override
            protected Long doInBackground(Object... objs) {

                try {
                    ppms = PPM.getAll(DataMap.getFactory(TrackFactory.class));
                } catch (Exception e) {
                    return new Long(-1);
                }

                return new Long(0);
            }

            @Override
            protected void onPostExecute(Long result){


                if(result == 0) {

                    ArrayList<PPM> objs = new ArrayList<PPM>();
                    for(int i=0;i<ppms.length;i++){
                        objs.add(ppms[i]);
                    }

                    BaseAdapter adapter = new PPMInstanceListAdapter(getActivity(),R.layout.list_ppm_instance,objs);
                    ppmList.setAdapter(adapter);



                }else{
                    Toast.makeText(getActivity().getApplicationContext(),"Error reading PPM list.",Toast.LENGTH_LONG).show();
                }
            }
        }

        try {
            //Toast.makeText(getActivity().getApplicationContext(),"Loading...",Toast.LENGTH_LONG).show();
            Load load = new Load();
            load.execute(this);

        }catch(Exception e){
            Toast.makeText(getActivity().getApplicationContext(),"Error reading PPM list.",Toast.LENGTH_LONG).show();
        }
    }
}
