package com.oddzod.track.PPM;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

import com.oddzod.track.R;


/**
 * Created by oddzod on 11/4/14.
 */
public class PPMMetricListAdapter extends ArrayAdapter<PPMMetricItem> {

    private Context context;
    private int layout;
    private List<PPMMetricItem> list;


    public PPMMetricListAdapter(Context context, int layout, List<PPMMetricItem> list) {
        super(context, layout, list);

        this.context=context;
        this.layout=layout;
        this.list=list;




    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return list.get(position).getView(position,convertView,parent,context);

    }
}
