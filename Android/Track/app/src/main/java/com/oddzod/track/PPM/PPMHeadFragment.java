package com.oddzod.track.PPM;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.oddzod.track.R;
import com.oddzod.track.model.ppm.PPM;

import butterknife.*;

public class PPMHeadFragment extends Fragment {

    @InjectView(R.id.wbvPPMHead)
    WebView wbvPPMHead;

    @InjectView(R.id.txvPPMTitle)
    TextView txvTitle;


    private com.oddzod.track.model.ppm.PPM ppm;

    public PPMHeadFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ppmhead, container, false);

        ButterKnife.inject(this,view);

        wbvPPMHead.loadData("<h2>Test</h2><br />this is a test <img src=\"http://cdn.wonderfulengineering.com/wp-content/uploads/2014/04/electric-motor-16.jpg\" width=\"300px\"/>  ","text/html","UTF8");


        updateUI();
        return view;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        updateUI();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setPPM(PPM PPM) {
        this.ppm = PPM;
        updateUI();
    }

    private void updateUI(){
        if(ppm != null){
            if(wbvPPMHead != null) {
                wbvPPMHead.loadData(ppm.getHead(), "text/html", "UTF8");
            }

            if(txvTitle != null){
                txvTitle.setText(ppm.getName());
            }

        }else{
            if(wbvPPMHead != null) {
                wbvPPMHead.loadData("", "text/html", "UTF8");
            }

            if(txvTitle != null){
                txvTitle.setText(ppm.getName());
            }
        }
    }

    public interface OnFragmentInteractionListener {

    }

    private OnFragmentInteractionListener mListener;

    public static PPMHeadFragment newInstance(String param1, String param2) {
        PPMHeadFragment fragment = new PPMHeadFragment();

        return fragment;
    }



}
