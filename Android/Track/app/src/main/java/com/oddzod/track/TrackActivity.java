package com.oddzod.track;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;

import com.oddzod.track.Home.HomeFragment;
import com.oddzod.track.Login.LoginFragment;
import com.oddzod.track.PPM.PPMFeedbackFragment;
import com.oddzod.track.PPM.PPMFragment;
import com.oddzod.track.PPM.PPMHeadFragment;
import com.oddzod.track.PPM.PPMInstanceSearchFragment;
import com.oddzod.track.PPM.PPMMetricFragment;
import com.oddzod.track.model.ppm.PPM;


public class TrackActivity extends Activity  implements  HomeFragment.OnFragmentInteractionListener, LoginFragment.OnFragmentInteractionListener,
                                                            PPMInstanceSearchFragment.OnFragmentInteractionListener,PPMFragment.OnFragmentInteractionListener,
                                                            PPMHeadFragment.OnFragmentInteractionListener,PPMMetricFragment.OnFragmentInteractionListener,
                                                            PPMFeedbackFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, HomeFragment.newInstance())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.track, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void openLogin() {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, LoginFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void openHome(){
        getFragmentManager().beginTransaction()
                .replace(R.id.container, HomeFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void openPPMInstanceSearch() {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, PPMInstanceSearchFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void openPPMInstance(PPM ppm) {
        PPMFragment frag = PPMFragment.newInstance();
        frag.setPPM(ppm);
        getFragmentManager().beginTransaction()
                .replace(R.id.container, frag)
                .addToBackStack(null)
                .commit();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_track, container, false);
            return rootView;
        }
    }
}
