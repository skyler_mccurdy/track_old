package com.oddzod.track.model.workOrder;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import com.oddzod.dataMap.baseType.ObjectBase;
import com.oddzod.dataMap.rest.actions.Delete;
import com.oddzod.dataMap.rest.actions.Get;
import com.oddzod.dataMap.rest.actions.Post;
import com.oddzod.dataMap.rest.actions.Put;
import com.oddzod.dataMap.rest.Property;
import com.oddzod.track.model.ppm.PPM;


@Get("workOrder/{"+WorkOrder.WORK_ORDER_ID+"}")
@Put("workOrder/{"+WorkOrder.WORK_ORDER_ID+"}")
@Post("workOrder/")
@Delete("workOrder/{"+WorkOrder.WORK_ORDER_ID+"}")
public class WorkOrder extends ObjectBase {

	public static final String WORK_ORDER_ID = "workOrder_ID";
	private int workOrder_ID = -1;
	
	@Property.Get(name = WORK_ORDER_ID)
	@Get.Parameter(WORK_ORDER_ID)
	@Put.Parameter(WORK_ORDER_ID)
	@Delete.Parameter(WORK_ORDER_ID)
	public int getWorkOrder_ID(){
		return workOrder_ID;
	}
	
	@Property.Set(name = WORK_ORDER_ID)
	public void setWorkOrder_ID(int workOrder_ID){
		this.workOrder_ID = workOrder_ID;
	}
	
	public static final String NAME = "name";
	private String name = null;
	
	@Property.Get(name = NAME)
	public String getName(){
		return name;
	}
	
	@Property.Set(name = NAME)
	public void setName(String name){
		this.name = name;
	}
	
	public static final String SCHEDULED_START_ON = "scheduledStartOn";
	private Date scheduledStartOn = null;
	
	@Property.Get(name = SCHEDULED_START_ON)
	public Date getScheduledStartOn(){
		return scheduledStartOn;
	}
	
	@Property.Set(name = SCHEDULED_START_ON)
	public void setScheduledStartOn(Date scheduledStartOn){
		this.scheduledStartOn = scheduledStartOn;
	}
	
	public static final String PPM_ID = "ppm_ID";
	private int ppm_ID = -1;
	
	@Property.Get(name = PPM_ID)
	public int getPPM_ID(){
		return ppm_ID;
	}
	
	@Property.Set(name = PPM_ID)
	public void setPPM_ID(int ppm_ID){
		this.ppm_ID = ppm_ID;
	}
	
	
	public static final String STATUS_ID = "status_ID";
	private int status_ID = -1;
	
	@Property.Get(name = STATUS_ID)
	public int getStatus_ID(){
		return status_ID;
	}
	
	@Property.Set(name = STATUS_ID)
	public void setStatus_ID(int status_ID){
		this.status_ID = status_ID;
	}
	
	
	public Status getStatus(){
		try {
			return (Status) getFactory().get(Status.class, getStatus_ID());
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | NoSuchMethodException
				| SecurityException | InvocationTargetException | IOException e) {
			return null;
		}
	}
	
	public PPM getPPM(){
		try {
			return (PPM) getFactory().get(PPM.class, getPPM_ID());
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | NoSuchMethodException
				| SecurityException | InvocationTargetException | IOException e) {
			return null;
		}
	}
	
	
	
	
	
	
	
	
	@Override
	public long getUniqueIdentifier() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	protected void setUniqueIdentifier(long uniqueIdentifier) {
		// TODO Auto-generated method stub

	}

}
