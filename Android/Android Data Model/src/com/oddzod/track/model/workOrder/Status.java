package com.oddzod.track.model.workOrder;

import com.oddzod.dataMap.baseType.ObjectBase;
import com.oddzod.dataMap.rest.actions.Delete;
import com.oddzod.dataMap.rest.actions.Get;
import com.oddzod.dataMap.rest.actions.Post;
import com.oddzod.dataMap.rest.actions.Put;
import com.oddzod.dataMap.rest.Property;


@Get("workOrder/status/{"+Status.STATUS_ID+"}")
@Put("workOrder/status/{"+Status.STATUS_ID+"}")
@Post("workOrder/status")
@Delete("workOrder/status/{"+Status.STATUS_ID+"}")
public class Status extends ObjectBase {

	public static final String STATUS_ID = "status_ID";
	private int status_ID = -1;
	
	@Property.Get(name = STATUS_ID)
	@Get.Parameter(STATUS_ID)
	@Put.Parameter(STATUS_ID)
	@Delete.Parameter(STATUS_ID)
	public int getStatus_ID(){
		return status_ID;
	}
	
	@Property.Set(name = STATUS_ID)
	public void setStatus_ID(int status_ID){
		this.status_ID = status_ID;
	}
	
	
	public static final String NAME = "name";
	private String name = null;
	
	@Property.Get(name = NAME)
	public String getName(){
		return name;
	}
	
	@Property.Set(name = NAME)
	public void setName(String name){
		this.name = name;
	}
	
	public static final String ORDER = "order";
	private int order = -1;
	
	@Property.Get(name = ORDER)
	public int getOrder(){
		return order;
	}
	
	@Property.Set(name = ORDER)
	public void setOrder(int order){
		this.order = order;
	}
	
	
	
	@Override
	public long getUniqueIdentifier() {
		return getStatus_ID();
	}

	@Override
	protected void setUniqueIdentifier(long uniqueIdentifier) {
		setStatus_ID((int)uniqueIdentifier);
	}

}
