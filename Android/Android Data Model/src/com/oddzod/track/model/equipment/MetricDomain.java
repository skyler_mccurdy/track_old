package com.oddzod.track.model.equipment;


import com.oddzod.dataMap.baseType.ObjectBase;
import com.oddzod.dataMap.rest.actions.Delete;
import com.oddzod.dataMap.rest.actions.Get;
import com.oddzod.dataMap.rest.actions.Post;
import com.oddzod.dataMap.rest.actions.Put;
import com.oddzod.dataMap.rest.Property;


@Get("equipment/metricDomain/{"+MetricDomain.METRIC_DOMAIN_ID+"}")
@Put("equipment/metricDomain/{"+MetricDomain.METRIC_DOMAIN_ID+"}")
@Post("equipment/metricDomain")
@Delete("equipment/metricDomain/{"+MetricDomain.METRIC_DOMAIN_ID+"}")
public class MetricDomain extends ObjectBase {

	
	public static final String METRIC_DOMAIN_ID = "metricDomain_ID";
	private int metricDomain_ID = -1;
	
	@Property.Get(name = METRIC_DOMAIN_ID)
	@Get.Parameter(METRIC_DOMAIN_ID)
	@Put.Parameter(METRIC_DOMAIN_ID)
	@Delete.Parameter(METRIC_DOMAIN_ID)
	public int getMetricDomain_ID(){
		return metricDomain_ID;
	}
	
	@Property.Set(name = METRIC_DOMAIN_ID)
	public void setMetricDomain_ID(int metricDomain_ID){
		this.metricDomain_ID = metricDomain_ID;
	}
	
	public static final String METRIC_ID = "metric_ID";
	private int metric_ID = -1;
	
	@Property.Get(name = METRIC_ID)
	public int getMetric_ID(){
		return metric_ID;
	}
	
	@Property.Set(name = METRIC_ID)
	public void setMetric_ID(int metric_ID){
		this.metric_ID = metric_ID;
	}
	
	public static final String ORDER = "order";
	private int order = -1;
	
	@Property.Get(name = ORDER)
	public int getOrder(){
		return order;
	}
	
	@Property.Set(name = ORDER)
	public void setOrder(int order){
		this.order = order;
	}
	
	public static final String NAME = "name";
	private String name = null;
	
	@Property.Get(name = NAME)
	public String getName(){
		return name;
	}
	
	@Property.Set(name = NAME)
	public void setName(String name){
		this.name = name;
	}
	
	public static final String VALUE = "value";
	private String value = null;
	
	@Property.Get(name = VALUE)
	public String getValue(){
		return value;
	}
	
	@Property.Set(name = VALUE)
	public void setValue(String value){
		this.value = value;
	}
	
	@Override
	public long getUniqueIdentifier() {
		return getMetricDomain_ID();
	}

	@Override
	protected void setUniqueIdentifier(long uniqueIdentifier) {
		setMetricDomain_ID((int)uniqueIdentifier);
	}

	public MetricDomain(){
		setNew(true);
		setModified(true);
	}
	
	public MetricDomain(Long id){
		setUniqueIdentifier(id);
		setNew(false);
		setModified(false);
	}
}
