package com.oddzod.track.model.equipment;

import java.util.Date;

import com.oddzod.dataMap.baseType.ObjectBase;
import com.oddzod.dataMap.rest.actions.Delete;
import com.oddzod.dataMap.rest.actions.Get;
import com.oddzod.dataMap.rest.actions.Post;
import com.oddzod.dataMap.rest.actions.Put;
import com.oddzod.dataMap.rest.Property;




@Get("equipment/metric/{"+MetricInspection.METRIC_ID+"}/inspection/{"+MetricInspection.METRIC_INSPECTION_ID+"}")
@Put("equipment/metric/{"+MetricInspection.METRIC_ID+"}/inspection/{"+MetricInspection.METRIC_INSPECTION_ID+"}")
@Post("equipment/metric/{"+MetricInspection.METRIC_ID+"}/inspection")
@Delete("equipment/metric/{"+MetricInspection.METRIC_ID+"}/inspection/{"+MetricInspection.METRIC_INSPECTION_ID+"}")
public class MetricInspection extends ObjectBase {

	public static final String METRIC_INSPECTION_ID = "metricInspection_ID";
	private int inspectionMetric_ID = -1;
	
	@Property.Get(name = METRIC_INSPECTION_ID)
	@Get.Parameter(METRIC_INSPECTION_ID)
	@Put.Parameter(METRIC_INSPECTION_ID)
	@Delete.Parameter(METRIC_INSPECTION_ID)
	public int getMetricInspection_ID(){
		return inspectionMetric_ID;
	}
	
	@Property.Set(name = METRIC_INSPECTION_ID)
	public void setMetricInspection_ID(int inspectionMetric_ID){
		this.inspectionMetric_ID = inspectionMetric_ID;
	}
	
	
	public static final String METRIC_ID = "metric_ID";
	private int metric_ID = -1;
	
	@Property.Get(name = METRIC_ID)
	@Get.Parameter(METRIC_ID)
	@Post.Parameter(METRIC_ID)
	@Put.Parameter(METRIC_ID)
	@Delete.Parameter(METRIC_ID)
	public int getmetric_ID(){
		return metric_ID;
	}
	
	@Property.Set(name = METRIC_ID)
	public void setMetric_ID(int metric_ID){
		this.metric_ID = metric_ID;
	}
	
	public static final String INSPECTED_ON = "inspectedOn";
	private Date inspectedOn = null;
	
	@Property.Get(name = INSPECTED_ON)
	public Date getInspectedOn(){
		return inspectedOn;
	}
	
	@Property.Set(name = INSPECTED_ON)
	public void setInspectedOn(Date inspectedOn){
		this.inspectedOn = inspectedOn;
	}
	
	public static final String INSPECTED_AT = "inspectedAt";
	private Double inspectedAt = null;
	
	@Property.Get(name = INSPECTED_AT)
	public Double getInspectedAt(){
		return inspectedAt;
	}
	
	@Property.Set(name = INSPECTED_AT)
	public void setInspectedAt(Double inspectedAt){
		this.inspectedAt = inspectedAt;
	}
	
	
	
	public static final String VALUE = "value";
	private String value = null;
	
	@Property.Get(name = VALUE)
	public String getValue(){
		return value;
	}
	
	public int getValueInt(){
		try{
			return Integer.parseInt(getValue());
		}catch(Exception e){
			return 0;
		}
	}
	
	public double getValueDouble(){
		try{
			return Double.parseDouble(getValue());
		}catch(Exception e){
			return 0;
		}
	}
	
	@Property.Set(name = VALUE)
	public void setValue(String value){
		this.value = value;
	}
	
	public void setValueInt(int value){
		setValue(Integer.toString(value));
	}
	
	public void setValueDouble(double value){
		setValue(Double.toString(value));
	}
	
	
	
	
	
	
	
	@Override
	public long getUniqueIdentifier() {
		return getMetricInspection_ID();
	}

	@Override
	protected void setUniqueIdentifier(long uniqueIdentifier) {
		setMetricInspection_ID((int)uniqueIdentifier);
	}
	
	public MetricInspection(){
		setNew(true);
		setModified(true);
	}
	
	public MetricInspection(Long id){
		setUniqueIdentifier(id);
		setNew(false);
		setModified(false);
	}

}
