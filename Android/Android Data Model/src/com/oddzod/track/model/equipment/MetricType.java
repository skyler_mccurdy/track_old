package com.oddzod.track.model.equipment;

import java.io.IOException;
import java.lang.reflect.Method;

import com.oddzod.dataMap.IDataFactory;
import com.oddzod.dataMap.baseType.ObjectBase;
import com.oddzod.dataMap.rest.actions.Delete;
import com.oddzod.dataMap.rest.actions.Get;
import com.oddzod.dataMap.rest.actions.GetArray;
import com.oddzod.dataMap.rest.actions.Post;
import com.oddzod.dataMap.rest.actions.Put;
import com.oddzod.dataMap.rest.Property;


@Get("equipment/metricType/{"+MetricType.METRIC_TYPE_ID+"}")
@Put("equipment/metricType/{"+MetricType.METRIC_TYPE_ID+"}")
@Post("equipment/metricType")
@Delete("equipment/metricType/{"+MetricType.METRIC_TYPE_ID+"}")
public class MetricType extends ObjectBase {
	
	@GetArray("equipment/metricType")
	public static MetricType[] getAll(IDataFactory factory) throws IllegalAccessException, InstantiationException, IOException{
		Method method;
		try {
			method = MetricType.class.getMethod("getAll", new Class<?>[]{IDataFactory.class});
		} catch (Exception e) {
			throw new IllegalAccessException("unable to retrive method information for equipment.MetricType.getAll");
		}
		return factory.getArray(MetricType.class,method);
	}
	

	public static final String METRIC_TYPE_ID = "metricType_ID";
	private int metricType_ID = -1;
	
	@Property.Get(name = METRIC_TYPE_ID)
	@Get.Parameter(METRIC_TYPE_ID)
	@Put.Parameter(METRIC_TYPE_ID)
	@Delete.Parameter(METRIC_TYPE_ID)
	public int getMetricType_ID(){
		return metricType_ID;
	}
	
	@Property.Set(name = METRIC_TYPE_ID)
	public void setMetricType_ID(int metricType_ID){
		this.metricType_ID = metricType_ID;
	}
	
	public static final String NAME = "name";
	private String name = null;
	
	@Property.Get(name = NAME)
	public String getName(){
		return name;
	}
	
	@Property.Set(name = NAME)
	public void setName(String name){
		this.name=name;
	}
	/*
	public static final String DATA_TYPE = "dataType";
	private String dataType = null;
	
	@Property.Get(name = DATA_TYPE)
	public String getDataType(){
		return dataType;
	}
	
	@Property.Set(name = DATA_TYPE)
	public void setDataType(String dataType){
		this.dataType = dataType;
	}
	*/
	public static final String IS_DISCREET = "isDiscreet";
	private boolean isDiscreet = false;
	
	@Property.Get(name = IS_DISCREET)
	public boolean getIsDiscreet(){
		return isDiscreet;
	}
	
	@Property.Set(name = IS_DISCREET)
	public void setIsDiscreet(boolean isDiscreet){
		this.isDiscreet = isDiscreet;
	}
	
	@Override
	public long getUniqueIdentifier() {
		return getMetricType_ID();
	}

	@Override
	protected void setUniqueIdentifier(long uniqueIdentifier) {
		setMetricType_ID((int)uniqueIdentifier);
	}

	public MetricType(){
		setNew(true);
		setModified(true);
	}
	
	public MetricType(Long id){
		setUniqueIdentifier(id);
		setNew(false);
		setModified(false);
	}
}
