package com.oddzod.track.model.equipment;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

import com.oddzod.dataMap.baseType.ObjectBase;
import com.oddzod.dataMap.rest.actions.Delete;
import com.oddzod.dataMap.rest.actions.Get;
import com.oddzod.dataMap.rest.actions.GetArray;
import com.oddzod.dataMap.rest.actions.Post;
import com.oddzod.dataMap.rest.actions.Put;
import com.oddzod.dataMap.rest.Property;



@Get("equipment/metric/{"+Metric.METRIC_ID+"}")
@Put("equipment/metric/{"+Metric.METRIC_ID+"}")
@Post("equipment/metric")
@Delete("equipment/metric/{"+Metric.METRIC_ID+"}")
public class Metric extends ObjectBase {

	public static final String METRIC_ID = "metric_ID";
	private int metric_ID = -1;
	
	@Property.Get(name = METRIC_ID)
	@Get.Parameter(METRIC_ID)
	@GetArray.Parameter(METRIC_ID)
	@Put.Parameter(METRIC_ID)
	@Delete.Parameter(METRIC_ID)
	public int getMetric_ID(){
		return metric_ID;
	}
	
	@Property.Set(name = METRIC_ID)
	public void setMetric_ID(int metric_ID){
		this.metric_ID = metric_ID;
	}
	
	public static final String NAME = "name";
	private String name = null;
	
	@Property.Get(name = NAME)
	public String getName(){
		return name;
	}
	
	@Property.Set(name = NAME)
	public void setName(String name){
		this.name = name;
	}
	
	public static final String METRIC_TYPE_ID = "metricType_ID";
	private int metricType_ID = -1;
	
	@Property.Get(name = METRIC_TYPE_ID)
	public int getMetricType_ID(){
		return metricType_ID;
	}
	
	@Property.Set(name = METRIC_TYPE_ID)
	public void setMetricType_ID(int metricType_ID){
		this.metricType_ID = metricType_ID;
	}
	
	public static final String MIN  = "min";
	private String min = null;
	
	@Property.Get(name = MIN)
	public String getMin(){
		return min;
	}
	
	public int getMinInt(){
		try{
			return Integer.parseInt(getMin());
		}catch(Exception e){
			return Integer.MIN_VALUE;
		}
	}
	
	public double getMinDouble(){
		try{
			return Double.parseDouble(getMin());
		}catch(Exception e){
			return Double.MIN_VALUE;
		}
	}
	
	@Property.Set(name = MIN)
	public void setMin(String min){
		this.min = min;
	}
	
	public static final String MAX = "max";
	private String max = null;
	
	@Property.Get(name = MAX)
	public String getMax(){
		return max;
	}
	
	public int getMaxInt(){
		try{
			return Integer.parseInt(getMax());
		}catch(Exception e){
			return Integer.MAX_VALUE;
		}
	}
	
	public double getMaxDouble(){
		try{
			return Double.parseDouble(getMax());
		}catch(Exception e){
			return Double.MAX_VALUE;
		}
	}
	
	@Property.Set(name = MAX)
	public void setMax(String max){
		this.max = max;
	}
	
	public static final String GRAPH_RANGE = "graphRange";
	private int graphRange = -1;
	
	@Property.Get(name = GRAPH_RANGE)
	public int getGraphRange(){
		return graphRange;
	}
	
	@Property.Set(name = GRAPH_RANGE)
	public void setGraphRange(int graphRange){
		this.graphRange = graphRange;
	}
	
	public static final String GRAPH_RANGE_TYPE = "graphRangeType";
	private int graphRangeType = -1;
	
	@Property.Get(name = GRAPH_RANGE_TYPE)
	public int getGraphRangeTypeInt(){
		return graphRangeType;
	}
	
	@Property.Set(name = GRAPH_RANGE_TYPE)
	public void setGraphRangeTypeInt(int graphRangeType){
		this.graphRangeType = graphRangeType;
	}
	
	public static enum GraphRangeType{
		NONE,
		DAYS,
		EQUIPMENT_HOURS
	}
	
	public GraphRangeType getGraphRangeType(){
		switch(getGraphRangeTypeInt()){
		default:
		case 0:
			return GraphRangeType.NONE;
		case 1:
			return GraphRangeType.DAYS;
		case 2:
			return GraphRangeType.EQUIPMENT_HOURS;
		}
	}
	
	public void setGraphRangeType(GraphRangeType type){
		switch(type){
		case NONE:
			setGraphRangeTypeInt(0);
			break;
		case DAYS:
			setGraphRangeTypeInt(1);
			break;
		case EQUIPMENT_HOURS:
			setGraphRangeTypeInt(2);
		}
	}
	
	
	public MetricType getMetricType(){
		try {
			return (MetricType) getFactory().get(MetricType.class, getMetricType_ID());
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | NoSuchMethodException
				| SecurityException | InvocationTargetException | IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@GetArray("equipment/metric/{"+METRIC_ID+"}/metricDomain")
	public MetricDomain[] getMetricDomain() throws IllegalAccessException, InstantiationException, IOException{
		Method method;
		try {
			method = this.getClass().getMethod("getMetricDomain", (Class<?>[])null);
		} catch (Exception e) {
			throw new IllegalAccessException("unable to retrive method information for equipment.Metric.getMetricDomain");
		}
		return getDataInterface().getArray(MetricDomain.class,method);
	}
	
	@GetArray("equipment/metric/{"+METRIC_ID+"}/inspection")
	public MetricInspection[] getInspections() throws IllegalAccessException, InstantiationException, IOException{
		Method method;
		try {
			method = this.getClass().getMethod("getInspections", (Class<?>[])null);
		} catch (Exception e) {
			throw new IllegalAccessException("unable to retrive method information for equipment.Metric.getInspections");
		}
		return getDataInterface().getArray(MetricInspection.class,method);
	}
	
	
	public MetricInspection createInspection(){
		try {
			MetricInspection insp =  (MetricInspection) getFactory().create(MetricInspection.class);
			insp.setInspectedOn(new Date());
			insp.setMetric_ID(getMetric_ID());
			
			return insp;
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | NoSuchMethodException
				| SecurityException | InvocationTargetException | IOException e) {
			return null;
		}
	}
	
	
	@Override
	public long getUniqueIdentifier() {
		return getMetric_ID();
	}

	@Override
	protected void setUniqueIdentifier(long uniqueIdentifier) {
		setMetric_ID((int)uniqueIdentifier);
	}
	
	public Metric(){
		setNew(true);
		setModified(true);
	}
	
	public Metric(Long id){
		setUniqueIdentifier(id);
		setNew(false);
		setModified(false);
	}

}
