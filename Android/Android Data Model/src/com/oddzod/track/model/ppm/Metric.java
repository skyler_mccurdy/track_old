package com.oddzod.track.model.ppm;


import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import com.oddzod.dataMap.baseType.ObjectBase;
import com.oddzod.dataMap.rest.actions.Delete;
import com.oddzod.dataMap.rest.actions.Get;
import com.oddzod.dataMap.rest.actions.Post;
import com.oddzod.dataMap.rest.actions.Put;
import com.oddzod.dataMap.rest.Property;


@Get("ppm/metric/{"+Metric.METRIC_ID+"}")
@Put("ppm/metric/{"+Metric.METRIC_ID+"}")
@Post("ppm/metric")
@Delete("ppm/metric/{"+Metric.METRIC_ID+"}")
public class Metric extends ObjectBase {
	
	public static final String METRIC_ID = "metric_ID";
	private int metric_ID = -1;
	
	@Property.Get(name = METRIC_ID)
	@Get.Parameter(METRIC_ID)
	@Put.Parameter(METRIC_ID)
	@Delete.Parameter(METRIC_ID)
	public int getMetric_ID(){
		return metric_ID;
	}
	
	@Property.Set(name = METRIC_ID)
	public void setMetric_ID(int metric_ID){
		this.metric_ID=metric_ID;
	}
	
	public static final String EQUIPMENT_METRIC_ID = "equipmentMetric_ID";
	private int equipmentMetric_ID = -1;
	
	@Property.Get(name = EQUIPMENT_METRIC_ID)
	public int getEquipmentMetric_ID(){
		return equipmentMetric_ID;
	}
	
	@Property.Set(name = EQUIPMENT_METRIC_ID)
	public void setEquipmentMetric_ID(int equipmentMetric_ID){
		this.equipmentMetric_ID=equipmentMetric_ID;
	}
	
	public static final String CATEGORY_ID = "category_ID";
	private int category_ID = -1;
	
	@Property.Get(name = CATEGORY_ID)
	public int getCategory_ID(){
		return category_ID;		
	}
	
	@Property.Set(name = CATEGORY_ID)
	public void setCategory_ID(int category_ID){
		this.category_ID = category_ID;
	}
	
	public static final String ORDER = "order";
	private int order = -1;
	
	@Property.Get(name = ORDER)
	public int getOrder(){
		return order;
	}
	
	@Property.Set(name = ORDER)
	public void setOrder(int order){
		this.order = order;
	}
	
	public static final String NOTE = "note";
	private String note = null;
	
	@Property.Get(name = NOTE)
	public String getNote(){
		return note;
	}
	
	@Property.Set(name = NOTE)
	public void setNote(String note){
		this.note=note;
	}
	
	
	public com.oddzod.track.model.equipment.Metric getEquipmentMetric(){
		try {
			return (com.oddzod.track.model.equipment.Metric) getFactory().get(com.oddzod.track.model.equipment.Metric.class, getEquipmentMetric_ID());
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | NoSuchMethodException
				| SecurityException | InvocationTargetException | IOException e) {
			return null;
		}
	}
	
	
	@Override
	public long getUniqueIdentifier() {
		return getMetric_ID();
	}

	@Override
	protected void setUniqueIdentifier(long uniqueIdentifier) {
		setMetric_ID((int)uniqueIdentifier);
	}
	
	public Metric(){
		setNew(true);
		setModified(true);
	}
	
	public Metric(Long id){
		setUniqueIdentifier(id);
		setNew(false);
		setModified(false);
	}

}
