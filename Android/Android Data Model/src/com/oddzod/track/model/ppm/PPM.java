package com.oddzod.track.model.ppm;

import java.io.IOException;
import java.lang.reflect.Method;

import com.oddzod.dataMap.IDataFactory;
import com.oddzod.dataMap.baseType.ObjectBase;
import com.oddzod.dataMap.rest.Property;
import com.oddzod.dataMap.rest.actions.Delete;
import com.oddzod.dataMap.rest.actions.Get;
import com.oddzod.dataMap.rest.actions.GetArray;
import com.oddzod.dataMap.rest.actions.Post;
import com.oddzod.dataMap.rest.actions.Put;


@Get("ppm/{ppm_ID}")
@Post("ppm")
@Put("ppm/{ppm_ID}")
@Delete("ppm/{ppm_ID}")
public class PPM extends ObjectBase{
	
	@GetArray("ppm")
	public static PPM[] getAll(IDataFactory factory) throws IllegalAccessException, InstantiationException, IOException{
		Method method;
		try {
			method = PPM.class.getMethod("getAll", new Class<?>[]{IDataFactory.class});
		} catch (Exception e) {
			throw new IllegalAccessException("unable to retrive method information for PPM.getAll");
		}
		return factory.getArray(PPM.class,method);
	}

	
	public static final String PPM_ID = "ppm_ID";
	private int ppm_ID = -1;
	
	@Property.Get(name=PPM_ID)
	@Get.Parameter(PPM_ID)
	@Put.Parameter(PPM_ID)
	@Delete.Parameter(PPM_ID)
	@GetArray.Parameter(PPM_ID)
	public int getPPM_ID(){
		return ppm_ID;
	}
	
	@Property.Set(name=PPM_ID)
	public void setPPM_ID(int ppm_ID){
		this.ppm_ID = ppm_ID;
	}
	
	
	public static final String NAME = "name";
	private String name = null;
	
	@Property.Get(name=NAME)
	public String getName(){
		return name;
	}
	
	@Property.Set(name=NAME)
	public void setName(String name){
		this.name=name;
	}
	
	public static final String HEAD = "head";
	private String head = null;
	
	@Property.Get(name=HEAD)
	public String getHead(){
		return head;
	}
	
	@Property.Set(name=HEAD)
	public void setHead(String head){
		this.head = head;
	}
	
	
	@GetArray("ppm/{"+PPM_ID+"}/metricCategories")
	public MetricCategory[] getMetricCategories() throws IllegalAccessException, InstantiationException, IOException{
		Method method;
		try {
			method = this.getClass().getMethod("getMetricCategories", (Class<?>[])null);
		} catch (Exception e) {
			throw new IllegalAccessException("unable to retrive method information for PPM.getMetricCategories");
		}
		return getDataInterface().getArray(MetricCategory.class,method);
	}
	
	
	
	@Override
	public long getUniqueIdentifier() {
		return getPPM_ID();
	}

	@Override
	protected void setUniqueIdentifier(long uniqueIdentifier) {
		setPPM_ID((int)uniqueIdentifier);
	}

	public PPM(){
		setNew(true);
		setModified(true);
	}
	
	public PPM(Long id){
		setUniqueIdentifier(id);
		setNew(false);
		setModified(false);
		
	}
}
