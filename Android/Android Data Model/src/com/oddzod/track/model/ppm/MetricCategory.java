package com.oddzod.track.model.ppm;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.oddzod.dataMap.baseType.ObjectBase;
import com.oddzod.dataMap.rest.actions.Delete;
import com.oddzod.dataMap.rest.actions.Get;
import com.oddzod.dataMap.rest.actions.GetArray;
import com.oddzod.dataMap.rest.actions.Post;
import com.oddzod.dataMap.rest.actions.Put;
import com.oddzod.dataMap.rest.Property;

@Get("ppm/metricCategory/{"+MetricCategory.CATEGORY_ID+"}")
@Post("ppm/metricCategory")
@Put("ppm/metricCategory/{"+MetricCategory.CATEGORY_ID+"}")
@Delete("ppm/metricCategory/{"+MetricCategory.CATEGORY_ID+"}")
public class MetricCategory extends ObjectBase {

	
	
	public static final String CATEGORY_ID = "metricCategory_ID";
	private int category_ID = -1;
	
	@Property.Get(name = CATEGORY_ID)
	@Get.Parameter(CATEGORY_ID)
	@Put.Parameter(CATEGORY_ID)
	@Delete.Parameter(CATEGORY_ID)
	@GetArray.Parameter(CATEGORY_ID)
	public int getCategory_ID(){
		return category_ID;
	}
	
	@Property.Set(name = CATEGORY_ID)
	public void setCategory_ID(int category_ID){
		this.category_ID = category_ID;
	}
	
	public static final String PPM_ID = "ppm_ID";
	private int ppm_ID = -1;
	
	@Property.Get(name = PPM_ID)
	public int getPPM_ID(){
		return ppm_ID;
	}
	
	@Property.Set(name = PPM_ID)
	public void setPPM_ID(int ppm_ID){
		this.ppm_ID = ppm_ID;
	}
	
	public static final String PARENT_ID = "parent_ID";
	private Integer parent_ID = null;
	
	@Property.Get(name=PARENT_ID)
	public Integer getParent_ID(){
		return parent_ID;
	}
	
	@Property.Set(name=PARENT_ID)
	public void setParent_ID(Integer parent_ID){
		this.parent_ID = parent_ID;
	}
	
	public static final String ORDER = "order";
	private int order;
	
	@Property.Get(name=ORDER)
	public int getOrder(){
		return order;
	}
	
	@Property.Set(name=ORDER)
	public void setOrder(int order){
		this.order = order;
	}
	
	public static final String NAME = "name";
	private String name = null;
	
	@Property.Get(name=NAME)
	public String getName(){
		return name;
	}
	
	@Property.Set(name=NAME)
	public void setName(String name){
		this.name = name;
	}
	
	public static final String NOTE = "note";
	private String note = null;
	
	@Property.Get(name=NOTE)
	public String getNote(){
		return note;
	}
	
	@Property.Set(name=NOTE)
	public void setNote(String note){
		this.note = note;
	}
	
	
	public PPM getPPM(){
		try {
			return (PPM) getFactory().get(PPM.class, getPPM_ID());
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | NoSuchMethodException
				| SecurityException | InvocationTargetException | IOException e) {
			return null;
		}
	}
	
	@GetArray("ppm/metricCategory/{"+CATEGORY_ID+"}/categories")
	public MetricCategory[] getCategories() throws IllegalAccessException, InstantiationException, IOException{
		Method method;
		try {
			method = MetricCategory.class.getMethod("getCategories", (Class<?>[])null);
		} catch (Exception e) {
			throw new IllegalAccessException("unable to retrive method information for ppm.MetricCategory.getCategories");
		}
		return getDataInterface().getArray(MetricCategory.class,method);
	}
	
	@GetArray("ppm/metricCategory/{"+CATEGORY_ID+"}/metrics")
	public Metric[] getMetrics() throws IllegalAccessException, InstantiationException, IOException{
		Method method;
		try {
			method = MetricCategory.class.getMethod("getMetrics", (Class<?>[])null);
		} catch (Exception e) {
			throw new IllegalAccessException("unable to retrive method information for ppm.MetricCategory.getMetrics");
		}
		return getDataInterface().getArray(Metric.class,method);
	}
	
	@Override
	public long getUniqueIdentifier() {
		return getCategory_ID();
	}

	@Override
	protected void setUniqueIdentifier(long uniqueIdentifier) {
		setCategory_ID((int)uniqueIdentifier);
	}
	
	public MetricCategory(){
		setNew(true);
		setModified(true);
	}
	
	public MetricCategory(Long id){
		setUniqueIdentifier(id);
		setNew(false);
		setModified(false);
	}

}
