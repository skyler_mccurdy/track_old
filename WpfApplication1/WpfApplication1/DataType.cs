﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    class DataType:Attribute
    {
        public Type value;
        public DataType(Type type)
        {
            this.value = type;
        }
    }
}
