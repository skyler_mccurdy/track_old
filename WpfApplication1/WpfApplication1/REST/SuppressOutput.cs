﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1.REST
{
    class SuppressOutput:Attribute
    {

        public const uint GET = 1;
        public const uint PUT = 2;
        public const uint POST = 4;
        public const uint DELETE = 8;

        public uint value { get; private set; }

        public SuppressOutput(uint value)
        {
            this.value = value;
        }

        public SuppressOutput()
        {
            this.value = 0xFFFFFFFF;
        }

    }
}
