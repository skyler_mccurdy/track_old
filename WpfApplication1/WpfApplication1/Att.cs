﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
    class Att:Attribute
    {
        public string[] values { get; private set; }

        public Att(string[] values)
        {
            this.values = values;
        }
    }
}
