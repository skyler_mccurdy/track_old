﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Web_API_Mock.Controllers
{
    public class MetricCategoryController : ApiController
    {
        [Route("ppm/{ppm_ID}/MetricCategory", Name = "MetricCategory_GetAll")]
        public MetricCategoryList GetMetricsForPPM(int ppm_ID)
        {
            UrlHelper urlHelper = new UrlHelper(Request);

            MetricCategoryList list = new MetricCategoryList();
            list.categories = Models.MetricCategory.getCategoriesForPPM(urlHelper,ppm_ID);

            list.metaData = new Dictionary<string, object>();

            MetaData.Action action = new MetaData.Action(urlHelper.Link("MetricCategory_GetAll", null));
            list.metaData.Add("get", action);

            action = new MetaData.Action(urlHelper.Link("MetricCategory_Create", new { ppm_ID = ppm_ID }));
            list.metaData.Add("create", action);


            return list;
        }

        [Route("ppm/{ppm_ID}/MetricCategory/{category_ID}", Name = "MetricCategory_GetByID")]
        public Models.MetricCategory Get(int ppm_ID,int category_ID)
        {
            List<Models.MetricCategory> cats = Models.MetricCategory.getCategoriesForPPM(
                new UrlHelper(Request),ppm_ID-1
                );


            Models.MetricCategory cat = findCategory(cats, category_ID);

            return cat;
        }


        private Models.MetricCategory findCategory(List<Models.MetricCategory> list,int category_ID)
        {
            if (list != null && list.Count > 0)
            {
                foreach (var c in list)
                {
                    if (c.category_ID == category_ID)
                    {
                        return c;
                    }
                    else
                    {
                        Models.MetricCategory result;
                        result = findCategory(c.categories, category_ID);
                        if (result != null)
                        {
                            return result;
                        }
                    }
                }
            }

            return null;
        }

        [Route("ppm/{ppm_ID}/MetricCategory", Name = "MetricCategory_Create")]
        public Models.MetricCategory Post(int ppm_ID,[FromBody]Models.MetricCategory value)
        {
            UrlHelper urlHelper = new UrlHelper(Request);

            Models.PPM ppm = Models.PPM.getPPMs(urlHelper)[ppm_ID - 1];

            Models.MetricCategory cat =
                Models.MetricCategory.createMetricCategory(urlHelper, ppm, null);

            cat.name = value.name;
            cat.note = value.note;
            cat.order = value.order;

            return cat;
        }
        /*
        [Route("ppm/{ppm_ID}/MetricCategory/{parent_ID}", Name = "MetricCategory_Create")]
        public Models.MetricCategory Post(int ppm_ID, int parent_ID, [FromBody]Models.MetricCategory value)
        {
            UrlHelper urlHelper = new UrlHelper(Request);

            Models.PPM ppm = Models.PPM.getPPMs(urlHelper)[ppm_ID - 1];
            Models.MetricCategory parent = findCategory(Models.MetricCategory.getCategoriesForPPM(urlHelper, ppm_ID), parent_ID);

            Models.MetricCategory cat =
                Models.MetricCategory.createMetricCategory(urlHelper, ppm, parent);

            cat.name = value.name;
            cat.note = value.note;
            cat.order = value.order;

            return cat;
        }
        */
        [Route("ppm/{ppm_ID}/MetricCategory/{category_ID}", Name = "MetricCategory_Update")]
        public Models.MetricCategory Put(int ppm_ID,int category_ID, [FromBody]Models.MetricCategory value)
        {
            Models.MetricCategory cat =
                findCategory(Models.MetricCategory.getCategoriesForPPM(new UrlHelper(Request), ppm_ID),
                    category_ID);

            cat.name = value.name;
            cat.note = value.note;
            cat.order = value.order;

            return cat;
        }

        [Route("ppm/{ppm_ID}/MetricCategory/{category_ID}", Name = "MetricCategory_Delete")]
        public string Delete(int ppm_ID, int category_ID)
        {
            return "Ok";
        }

        public class MetricCategoryList
        {
            public Dictionary<string, object> metaData;

            public List<Models.MetricCategory> categories;
        }
    }
}
