﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Routing;

namespace Web_API_Mock.Controllers
{
    public class PPMController : ApiController
    {

        [Route("ppm", Name="PPM_GetAll")]
        public PPMList Get()
        {
            UrlHelper urlHelper = new UrlHelper(Request);

            PPMList ppms = new PPMList();
            ppms.ppms = Models.PPM.getPPMs(urlHelper);
            ppms.metaData = new Dictionary<string, object>();

            MetaData.Action action = new MetaData.Action(urlHelper.Link("PPM_GetAll", null));
            action.options.Add("page");
            action.options.Add("pageSize");
            action.options.Add("name");
            action.options.Add("mp2_PPM");
            ppms.metaData.Add("get",action);

            action = new MetaData.Action(urlHelper.Link("PPM_Create", null));
            ppms.metaData.Add("create", action);

            return ppms;
        }

        [Route("ppm/{id}", Name="PPM_GetByID")]
        public Models.PPM Get(int id)
        {
            return Models.PPM.getPPMs(new UrlHelper(Request))[id-1];
        }

        [Route("ppm", Name="PPM_Create")]
        public Models.PPM Post([FromBody]Models.PPM value)
        {
            Models.PPM ppm = Models.PPM.createNew(new UrlHelper(Request));

            ppm.name = value.name;
            ppm.head = value.head;
            ppm.mp2_PPM = value.mp2_PPM;



            return ppm;

        }

        [Route("ppm/{id}", Name="PPM_Update")]
        public Models.PPM Put(int id, [FromBody]Models.PPM value)
        {
            Models.PPM ppm = Models.PPM.getPPMs(new UrlHelper(Request))[id-1];

            
            ppm.name = value.name;
            ppm.head = value.head;
            ppm.mp2_PPM = value.mp2_PPM;

            return ppm;
        }

        [Route("ppm/{id}", Name="PPM_Delete")]
        public string Delete(int id)
        {
            return "ok";
        }

        

        public class PPMList
        {
            public Dictionary<string, object> metaData;
            public List<Models.PPM> ppms;
        }


        
    }
}
