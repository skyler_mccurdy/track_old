﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_API_Mock.MetaData
{
    public class Action
    {
        public string url { get; set; }
        public List<string> options { get; set; }

        public Action(string url)
        {
            this.url = url;
            this.options = new List<string>();
        }

        public Action(string url, List<string> options)
        {
            this.url = url;
            this.options = options;
        }
    }
}