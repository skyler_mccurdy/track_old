﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;

namespace Web_API_Mock.Models
{
  
    public class PPM
    {
        public Dictionary<string, object> metaData;

        public int ppm_ID { get; set; }
        public string name { get; set; }
        public string head { get; set; }
        public string mp2_PPM { get; set; }


        private static List<PPM> list = null;

        public static PPM createNew(UrlHelper urlHelper)
        {
            if (list == null)
            {
                list = getPPMs(urlHelper);
            }

            list.Add(new Models.PPM());
            list[list.Count - 1].ppm_ID = list.Count;

            list[list.Count - 1].metaData = new Dictionary<string, object>();
            list[list.Count - 1].metaData.Add("get", new MetaData.Action(urlHelper.Link("PPM_GetByID", new { id = list[0].ppm_ID })));
            list[list.Count - 1].metaData.Add("update", new MetaData.Action(urlHelper.Link("PPM_Update", new { id = list[0].ppm_ID })));
            list[list.Count - 1].metaData.Add("delete", new MetaData.Action(urlHelper.Link("PPM_Delete", new { id = list[0].ppm_ID })));

            return list[list.Count-1];
        }

        public static List<PPM> getPPMs(UrlHelper urlHelper)
        {
            list = new List<PPM>();
            PPM ppm = createNew(urlHelper);

                
            ppm.name = "Test PPM 1";
            ppm.head = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam blandit varius sodales. Nulla maximus ac nisi ac euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id aliquet dolor, vel ultricies eros. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas id fringilla eros, in ultrices est. Vivamus lobortis est magna, quis efficitur sapien volutpat ut. Proin feugiat mattis lacus, in vulputate magna faucibus non. Suspendisse dui odio, euismod sit amet nisl et, consequat ornare nulla. Etiam lacinia sodales nunc non tincidunt. Sed vel odio pretium, efficitur quam non, malesuada libero. Aliquam congue sapien augue, ut consequat sem lacinia nec." +
                            "Proin tempus erat risus, nec porttitor dui mollis eget. Etiam cursus tortor ac turpis placerat rhoncus. In tristique vel felis quis varius. Proin porta consequat condimentum. Mauris nisl justo, porttitor vel augue ut, vehicula elementum tortor. Ut fermentum fringilla libero, vitae commodo nisi fermentum non. Aenean vel nulla non nisi euismod auctor sed quis dui. Aenean et finibus urna, scelerisque tempus nunc. Quisque quis ante ac mauris aliquam posuere vel a nisi. Nam placerat sodales velit ornare dictum. Pellentesque pellentesque eget orci ut dapibus. Nullam vestibulum, libero vitae molestie rhoncus, purus lectus vulputate est, eu rhoncus mauris lacus at lorem. Fusce ullamcorper condimentum hendrerit." +
                            "Sed eleifend fermentum efficitur. Nunc dignissim iaculis urna, eu placerat neque porttitor vitae. Duis sit amet dui enim. Nunc sapien ante, facilisis id sodales nec, maximus non nunc. Suspendisse potenti. Sed enim velit, auctor vitae ex nec, viverra facilisis erat. Vivamus at rutrum nunc, at mattis odio. Aenean feugiat placerat ante, luctus pharetra metus laoreet non. Donec congue non nisl a maximus. Nullam scelerisque egestas maximus. Cras condimentum, est consequat vehicula aliquam, lectus tortor efficitur turpis, eu vulputate neque eros at ex. Etiam sit amet est dictum, sollicitudin dolor ac, cursus velit. Phasellus venenatis arcu turpis, ac elementum tellus finibus tincidunt. Curabitur eget scelerisque ipsum. Ut iaculis egestas blandit. Etiam hendrerit, ipsum vel malesuada fermentum, felis dui vulputate nulla, in pulvinar eros ipsum sit amet sapien." +
                            "Nulla eget tellus non sapien lobortis laoreet. Integer pellentesque et lorem id cursus. Nunc volutpat varius nunc in efficitur. Nullam rutrum tempor felis, et facilisis lectus efficitur et. Ut sollicitudin purus sed tellus ultrices, et viverra quam sagittis. Curabitur dignissim tellus id erat vestibulum, vitae tincidunt nulla molestie. Proin ex turpis, consectetur sed orci eu, aliquet sagittis massa. Proin egestas nec est maximus pulvinar. Ut et urna posuere augue facilisis tincidunt ac ut magna. Morbi pellentesque rutrum vulputate." +
                            "Vivamus erat nibh, ultrices a sem in, pharetra sagittis elit. Proin ex orci, semper quis diam vel, lobortis ullamcorper libero. Proin quis rhoncus ipsum. Sed faucibus ac risus vel ullamcorper. Nulla nisl odio, convallis a lectus tempor, mollis dapibus sapien. Proin erat ex, mattis non ipsum ac, laoreet viverra nulla. Donec volutpat id tellus eu fermentum. Donec luctus mattis tempor. Integer nec tristique felis. Phasellus finibus augue nec tellus accumsan, ut maximus odio iaculis. Proin lacinia feugiat lorem, ut imperdiet massa volutpat id. Praesent placerat risus arcu, sit amet pellentesque ex dignissim sed. Donec faucibus bibendum aliquet. Nullam at ante vitae magna tristique suscipit.";
            ppm.mp2_PPM = "PPM-T01-001-1Q";


            ppm = createNew(urlHelper);
            ppm.name = "Test PPM 2";
            ppm.head = "<div class=\"node-body article-body\"><p>There's typically no way around it. At some time during a motor's life, it is likely to experience high vibration levels. What's behind this maintenance phenomenon? The cause may be an electromagnetic or mechanical imbalance, loose components, rubbing parts, bearing failure, or resonance.</p>" +
                            "<p>When a problem occurs, it is critical to quickly identify the source of the trouble and take corrective action. Using present-day diagnostic techniques can save you money and downtime.</p>" +
                            "<p>Vibration levels shown in the Table below are considered excessive. If you find these levels during inspection, quickly resolve the situation to ensure you don't sacrifice motor reliability. Levels that are half as high as those in the table indicate excellent machine operation.</p>" +
                            "<h3>                              	Shaft vibration vs. housing vibration</h3>" +
                            "<p>The necessity of obtaining housing vibration data vs. shaft vibration data depends on the problem. It is often desirable to obtain both, especially on a machine with sleeve bearings. If the problem originates in the housing or motor frame (for example, twice line frequency vibration), gather housing vibration data with magnetically mounted accelerometers. If the problem originates in the rotor (unbalance or oil whirl, for instance), obtain shaft vibration data with either a shaft stick or a proximity probe.</p>" +
                            "<p>Proximity probes yield vibration data of the shaft movement relative to the housing, whereas shaft stick measurements yield vibration data based on an absolute reference location. Obtain shaft stick measurements by placing a handheld accelero-meter attached to a wood fishtail stick against a smooth rotating surface on the motor shaft. If the motor comes with proximity probes, use them. If it does not and relative shaft vibration is required, set up proximity probes with magnetic mounts. Always obtain housing vibration data with an absolute reference.</p>" +
                            "<h3>                              	Take the correct direction of measurement</h3>" +
                            "<p>You should take housing vibration measurements in three planes (vertical, horizontal, and axial) on both bearing housings, as shown in the Figure on the right. Take shaft vibration measurements in two directions (90° apart) on one drive end of the motor. If the motor has proximity probes, you typically mount them at a 45° offset from both sides of the upper vertical centerline.</p>" +
                            "<p>You can measure vibration in units of displacement (peak-to-peak movement in mils or thousands of an inch), units of velocity (zero-to-peak in in./sec), or units of acceleration (zero-to-peak in gs). Acceleration emphasizes high frequencies, displacement emphasizes low frequencies, and velocity gives equal emphasis to all frequencies. Troubleshooters commonly use displacement units for shaft vibration measurement and velocity units for housing vibration measurement.</p>" +
                            "<h3>                              	Recommended troubleshooting procedure</h3>" +
                            "<p>If you suspect high vibration may be the source of your troubles, various tests can pinpoint the trouble spot. But first, you should perform some basic maintenance checks to answer these questions:</p>" +
                            "<ul>                              	<li>                              		<p>Are all the bolts tight?</p>                              	</li>                              	<li>                              		<p>Have you eliminated “soft foot”? Check for it by loosening the four hold-down bolts that pass through the motor mounting feet one at a time. A dial indicator should show no more than .001 in. of movement in the vertical direction of the mounting foot. Install shims to eliminate the situation.</p>                              	</li>" +
                            "<li>                              		<p>Is hot (or operating) alignment good? If it's not possible to verify hot alignment, have you verified cold motor (or not-operating) alignment with appropriate thermal compensation checks?</p>                              	</li>                              	<li>                              		<p>Is any part of the system (such as the junction box top cover or piping) vibrating excessively? Are any of the parts attached to the motor resonant?</p>                              	</li>" +
                            "<li>                              		<p>Is the foundation or the motor frame vibrating at a higher level (more than 25%) than the motor? If so, there could be a weak motor base.</p>                              	</li>                              	<li>                              		<p>Are any parts of the motor or shaft loose?</p>                              	</li>                              	<li>                              		<p>Have any fan blades eroded or broken off? Is coupling lubrication satisfactory?</p>" +
                            "<p>If the above items are satisfactory, but vibration remains high, perform a thorough vibration analysis.</p>                              		<p>You can diagnose a vibration problem in two steps. First, obtain vibration data, but remember factors like noise, combined signals, and amplitude modulation can influence the data. Then, determine what operating conditions increase, decrease, or have no effect on vibration. Obtain vibration measurements with the motor operating under the following conditions:</p>                              	</li>" +
                            "<li>                              		<p><em>Loaded, coupled, full voltage, all conditions stabilized</em>. The first measurements you take should represent the machine under normal operation and may indicate the tests you should perform next.</p>                              	</li>                              	<li>                              		<p><em>Unloaded, coupled, full voltage</em>. This condition removes some, but not all, load-related vibration.</p>                              	</li>                              	" +
                            "<li>                              		<p><em>Unloaded, uncoupled</em>, full voltage. This condition removes all effects of the coupling and mechanical load driven by the motor. It isolates the motor/base system from the measurement.</p>                              	</li>                              	<li>                              		<p><em>Unloaded, uncoupled, reduced voltage (25% if possible)</em>. This test minimizes the effect of magnetic pullover. A 25% voltage reduction is usually only possible at a motor service shop or manufacturer's facility. If the motor is a Y-Delta connected machine, the Y connection is effectively 57% voltage as compared to the D connection at the same terminal voltage. A comparison of vibration under both connections reveals the voltage sensitivity of the motor.</p>" +
                            "</li>                              	<li>                              		<p><em>Unloaded, uncoupled, coast down</em>. This test reveals any resonance/critical-speed problem for the entire motor/base/driven equipment system.</p>                              	</li>                              </ul>" +
                            "<p>More stringent specifications regarding motor vibration call for maximum velocity levels of 0.1 in./sec on the housing and 1.5 mils of displacement vibration on the shaft. The limits apply to a motor mounted on a seismic mass, and uncoupled or coupled to a piece of equipment in a way that eliminates any vibration in the driven equipment.</p>" +
                            "<p>Vibration problems can vary from a mere nuisance to an indication of imminent motor failure. With a solid knowledge of motor fundamentals and vibration analysis, you can usually identify areas of concern well before they lead to a failure.</p>                              <p><em>Finley is manager of engineering and Hodowanec is manager of mechanical engineering in the Large Motor Business division of Siemens Energy &amp; Automation, Inc. in Norwood, Ohio.</em></p></div>";
            ppm.mp2_PPM = "PPM-T01-002-A";

            return list;
        }
    }
   
}