﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;

namespace Web_API_Mock.Models
{
    public class MetricCategory
    {
        public Dictionary<string, object> metaData;

        public int category_ID { get; set; }
        public int ppm_ID { get; set; }
        public int? parent_ID { get; set; }
        public int order { get; set; }
        public string name { get; set; }
        public string note { get; set; }

        public Dictionary<string,object> ppm { get; set; }
        public Dictionary<string, object> parent { get; set; }

        public List<MetricCategory> categories { get; set; }
        public List<Metric> metrics { get; set; }


        private static Dictionary<int, List<MetricCategory>> cats = new Dictionary<int, List<MetricCategory>>();


        public static List<MetricCategory> getCategoriesForPPM(UrlHelper urlHelper,int ppm_ID)
        {
            if (!cats.ContainsKey(ppm_ID))
            {
                generateCategoriesForPPM(urlHelper, ppm_ID);
            }
            return cats[ppm_ID];
        }

        private static void generateCategoriesForPPM(UrlHelper urlHelper, int ppm_ID)
        {
            List<MetricCategory> list = new List<MetricCategory>();
            PPM ppm = PPM.getPPMs(urlHelper)[ppm_ID];
            for (int i = 0; i < 3; i++)
            {
                MetricCategory c = createMetricCategory(urlHelper,ppm,null);
                c.order = i;
                c.name = (i + 1) + " Category Name";
                c.note = "Some Notes";

                c.categories = new List<MetricCategory>();
                for (int x = 0; x < 3; x++)
                {
                    generateCategoriesForPPM(urlHelper, ppm, c, 3);
                }

                list.Add(c);
            }


            cats.Add(ppm_ID, list);
        }

        private static void generateCategoriesForPPM(UrlHelper urlHelper,PPM ppm, MetricCategory parent,int depth)
        {
            if (depth > 0)
            {
                for (int i = 0; i < 1; i++)
                {
                    MetricCategory c = createMetricCategory(urlHelper,ppm,parent);
                    c.order = i;
                    c.name = (i + 1) + " Category Name";
                    c.note = "Some Notes";

                    c.categories = new List<MetricCategory>();
                    for (int x = 0; x < 3; x++)
                    {
                        generateCategoriesForPPM(urlHelper, ppm, c, depth-1);
                    }

                    parent.categories.Add(c);
                }
            }
        }

        private static int cat_ID = 1;

        public static MetricCategory createMetricCategory(UrlHelper urlHelper, PPM ppm,MetricCategory parent)
        {
            MetricCategory c = new MetricCategory();
            c.category_ID = cat_ID++;
            c.ppm_ID = ppm.ppm_ID;
            c.ppm = ppm.metaData;

            c.parent_ID = null;
            c.parent = null;

            if (parent != null)
            {
                c.parent_ID = parent.parent_ID;
                c.parent = parent.metaData;
            }

            c.metaData = new Dictionary<string, object>();
            c.metaData.Add("get", new MetaData.Action(urlHelper.Link("MetricCategory_GetByID", new { ppm_ID=c.ppm_ID, category_ID = c.category_ID })));
            c.metaData.Add("update", new MetaData.Action(urlHelper.Link("MetricCategory_Update", new { ppm_ID = c.ppm_ID, category_ID = c.category_ID })));
            c.metaData.Add("delete", new MetaData.Action(urlHelper.Link("MetricCategory_Delete", new { ppm_ID = c.ppm_ID, category_ID = c.category_ID })));


            return c;
        }
    }
}